package com.maxffort.newsapp.tex;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceScreen;
import android.support.annotation.Nullable;

import android.util.Log;
import android.widget.Toast;



public class SettingsFragment extends PreferenceFragment  {
    private static final String TAG = "SettingsFragment";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



        addPreferencesFromResource(R.xml.preferences);

    }

    @Override
    public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen, Preference preference) {
        String key = preference.getKey();
        if (key.equals("invites")){
            Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            String content = "https://play.google.com/store/apps/details?id=com.maxffort.newsapp.tex";
            sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Subject");
            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, content);
            startActivity(Intent.createChooser(sharingIntent, "Share via"));
        }

        else if (key.equals("new")){
            new AlertDialog.Builder(getActivity())
                    .setTitle("What's New").setMessage("1. UI changes")


                    .setPositiveButton("Done", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    })

                    .show();
        }

        return false;
    }

//    @Override
//    public boolean onPreferenceClick(Preference preference) {
//
//
//        Preference pref = findPreference("logout");
//
//        pref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
//            @Override
//            public boolean onPreferenceClick(Preference preference) {
//                Intent intent = new Intent(getActivity().getApplicationContext(),LoginActivity.class);
//                startActivity(intent);
//                getActivity().finish();
//                return true;
//            }
//        });
//
//        return false;
//    }
}
