package com.maxffort.newsapp.tex.helperclasses;

public class FactComment {
    private String uname, ucomment, uimgurl;

    public FactComment(){

    }

    public FactComment(String uname, String ucomment, String uimgurl){
        this.uname = uname;
        this.ucomment = ucomment;
        this.uimgurl = uimgurl;
    }

    public String getUname() {
        return uname;
    }

    public void setUname(String uname) {
        this.uname = uname;
    }

    public String getUcomment() {
        return ucomment;
    }

    public void setUcomment(String ucomment) {
        this.ucomment = ucomment;
    }

    public String getUimgurl() {
        return uimgurl;
    }

    public void setUimgurl(String uimgurl) {
        this.uimgurl = uimgurl;
    }
}
