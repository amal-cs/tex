package com.maxffort.newsapp.tex.helperclasses;

public class Comment {

    private String name,comment,urlImg;

    public Comment(){

    }

    public Comment(String name,String comment,String urlImg){
        this.name = name;
        this.comment = comment;
        this.urlImg = urlImg;
    }


    public String getName() {
        return name;
    }

    public String getComment() {
        return comment;
    }

    public String getUrlImg() {
        return urlImg;
    }
}
