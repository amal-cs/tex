package com.maxffort.newsapp.tex.helperclasses;

public class Feed {

    private String heading, imageUrl, newsId, description, date, link,timeStamp;
    private int commentCount, likeCount;

    public Feed(String heading, String imageUrl, int commentCount, int likeCount, String newsId, String description, String date, String link,String timeStamp) {
        this.heading = heading;
        this.link = link;
        this.imageUrl = imageUrl;
        this.newsId = newsId;
        this.description = description;
        this.commentCount = commentCount;
        this.likeCount = likeCount;
        this.date = date;
        this.timeStamp = timeStamp;
    }
    public String getTimeStamp(){return timeStamp;}

    public void setTimeStamp(String timeStamp){
        this.timeStamp = timeStamp;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getNewsId() {
        return newsId;
    }

    public void setNewsId(String newsId) {
        this.newsId = newsId;
    }

    public String getHeading() {
        return heading;
    }

    public void setHeading(String heading) {
        this.heading = heading;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public int getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(int commentCount) {
        this.commentCount = commentCount;
    }

    public int getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(int likeCount) {
        this.likeCount = likeCount;
    }
}
