package com.maxffort.newsapp.tex;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.design.widget.AppBarLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;
import android.widget.Toolbar;

import com.google.ads.consent.ConsentInformation;
import com.google.ads.consent.ConsentStatus;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.maxffort.newsapp.tex.adapter.FeedAdapter;

public class SourceWebView extends AppCompatActivity {

    private WebView webView;
    private ActionBar actionBar;
//    private AdView adView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_source_web_view);

        webView = findViewById(R.id.sourceWebView);
//        adView = findViewById(R.id.webViewAdView);

        if (getSupportActionBar() != null)
            getSupportActionBar().hide();

        String link = getIntent().getStringExtra("link");



        webView.setWebViewClient(new MyBrowser());

        webView.getSettings().setLoadsImagesAutomatically(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        webView.loadUrl(link);

        ConsentInformation consentInformation = ConsentInformation.getInstance(getApplicationContext());
        Bundle extras = new Bundle();

//        if (consentInformation.getConsentStatus().equals(ConsentStatus.NON_PERSONALIZED)) {
//            extras.putString("npa", "1");
//            AdRequest adRequest = new AdRequest.Builder()
//                    .addNetworkExtrasBundle(AdMobAdapter.class, extras)
//                    .build();
//            adView.loadAd(adRequest);
//
//        } else {
//            AdRequest adRequest = new AdRequest.Builder().build();
//           adView.loadAd(adRequest);
//        }



    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && this.webView.canGoBack()) {
            this.webView.goBack();
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

    private class MyBrowser extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }

}
