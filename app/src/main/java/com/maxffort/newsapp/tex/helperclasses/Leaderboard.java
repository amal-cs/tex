package com.maxffort.newsapp.tex.helperclasses;

import java.util.Comparator;

public class Leaderboard {

    private String userId;
    private String time;

    public Leaderboard(){

    }

    public Leaderboard(String userId,String time){
        this.userId = userId;
        this.time = time;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

}
