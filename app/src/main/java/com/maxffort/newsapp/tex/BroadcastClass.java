package com.maxffort.newsapp.tex;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import android.os.Build;
import android.widget.Toast;


public class BroadcastClass extends BroadcastReceiver {
    String heading;

    @Override
    public void onReceive(Context context, Intent intent) {


        heading = intent.getStringExtra("Heading");
        showNotification(context,heading);
    }


    private void showNotification(Context context,String heading) {
        PendingIntent contentIntent = PendingIntent.getActivity(context, 0,
                new Intent(context, MenuActivity.class), 0);

        Notification.Builder mBuilder =
                new Notification.Builder(context)
                        .setContentTitle("Event Alert").setContentText(heading);
        mBuilder.setSmallIcon(getNotificationIcon());
        mBuilder.setContentIntent(contentIntent);
        mBuilder.setDefaults(Notification.DEFAULT_SOUND);
        mBuilder.setAutoCancel(true);
        NotificationManager mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);



        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String channelId = "201";
            NotificationChannel channel = new NotificationChannel(channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT);
            mNotificationManager.createNotificationChannel(channel);
            mBuilder.setChannelId(channelId);
        }

        mNotificationManager.notify(0, mBuilder.build());

    }

    private int getNotificationIcon() {
        boolean useWhiteIcon = (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP);
        return useWhiteIcon ? R.drawable.notification_icon : R.mipmap.logo_round;
    }
}
