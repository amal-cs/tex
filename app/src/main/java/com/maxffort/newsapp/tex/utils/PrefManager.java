package com.maxffort.newsapp.tex.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class PrefManager {
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor ;
    Context context;

    int PRIVATE_MODE = 0;
    private  static final String PREF_NAME = "intro";
    private static final String  IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch";
    public PrefManager(Context context){
        this.context = context;
        sharedPreferences = context.getSharedPreferences(PREF_NAME,PRIVATE_MODE);
        editor = sharedPreferences.edit();

    }

    public void setIsFirstTimeLaunch(Boolean isFirstTIme){
        editor.putBoolean(IS_FIRST_TIME_LAUNCH,isFirstTIme);
        editor.commit();
    }
    public boolean isFirstTimelaunch(){
        return sharedPreferences.getBoolean(IS_FIRST_TIME_LAUNCH,true);
    }


}
