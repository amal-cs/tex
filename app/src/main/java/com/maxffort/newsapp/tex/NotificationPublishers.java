package com.maxffort.newsapp.tex;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.widget.Toast;

public class NotificationPublishers extends BroadcastReceiver {
    public static String NOTIFICATION_TITLE = "notification-title";
    public static String NOTIFICATION_ID = "notification-id";
    public static String NOTIFICATION_DESCRIPTION = "notification-desc";
    @Override
    public void onReceive(Context context, Intent intent) {
        String title = intent.getStringExtra(NOTIFICATION_TITLE);
        String id = intent.getStringExtra(NOTIFICATION_ID);
        String description = intent.getStringExtra(NOTIFICATION_DESCRIPTION);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Intent resultIntent = new Intent(context,SnoozeActivity.class);
        resultIntent.putExtra("notificationHeading",id);
        PendingIntent resultPendingIntent = PendingIntent.getActivity(context, 0,resultIntent,0);




        Notification.Builder builder =  new Notification.Builder(context);
        builder.setContentTitle(title);
        builder.setContentText(description);
        builder.setSmallIcon(getNotificationIcon());
        builder.setSound(defaultSoundUri);
        builder.setAutoCancel(true);
        builder.setContentIntent(resultPendingIntent);



        NotificationManager notificationManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String channelId = "201";
            NotificationChannel channel = new NotificationChannel(channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
            builder.setChannelId(channelId);
        }
        notificationManager.notify(0, builder.build());
    }
    private int getNotificationIcon() {
        boolean useWhiteIcon = (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP);
        return useWhiteIcon ? R.drawable.notification_icon : R.mipmap.logo_round;
    }
}
