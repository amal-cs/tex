package com.maxffort.newsapp.tex;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.maxffort.newsapp.tex.adapter.CommentAdapter;
import com.maxffort.newsapp.tex.adapter.FeedAdapter;
import com.maxffort.newsapp.tex.helperclasses.Comment;
import com.maxffort.newsapp.tex.helperclasses.Facts;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;

public class CommentActivity extends AppCompatActivity {

    private ArrayList<Comment> commentArrayList = new ArrayList<>();
    private CommentAdapter commentAdapter;
    private DatabaseReference databaseReference;
    private EditText commentEditText;
    private ImageView sendCommentButton;
    private int i;
    private GoogleSignInAccount googleSignInAccount;
    private TextView nocomment;
    private RecyclerView recyclerView;
    private static final String TAG = "CommentActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);

        nocomment = findViewById(R.id.nocomment);
        commentEditText = findViewById(R.id.commentEditText);
        sendCommentButton = findViewById(R.id.sendCommentButton);
        googleSignInAccount = GoogleSignIn.getLastSignedInAccount(getApplicationContext());

        Calendar calendar = Calendar.getInstance();
        final String date = Integer.toString(calendar.get(Calendar.DATE)) + "-" + Integer.toString(calendar.get(Calendar.MONTH) + 1) + "-" + Integer.toString(calendar.get(Calendar.YEAR));

        recyclerView = findViewById(R.id.commentRecyclerView);
        String id = getIntent().getStringExtra("NewsId");
        i = getIntent().getIntExtra("position", 0);


        databaseReference = FirebaseDatabase.getInstance().getReference("News/" + id + "/Comments");
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                    Log.d(TAG, "onDataChange: " + dataSnapshot1);
                    commentArrayList.add(dataSnapshot1.getValue(Comment.class));
                }

                if (commentArrayList.size() == 0){
                    nocomment.setVisibility(View.VISIBLE);
                }

                Collections.reverse(commentArrayList);

                commentAdapter = new CommentAdapter(CommentActivity.this, commentArrayList);

                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
                recyclerView.setLayoutManager(layoutManager);
                recyclerView.setAdapter(commentAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        sendCommentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String comment = commentEditText.getText().toString();
                nocomment.setVisibility(View.GONE);
                if (!comment.isEmpty() && FirebaseAuth.getInstance().getUid() != null) {
                    pushValues(comment);
                }

            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(CommentActivity.this, MenuActivity.class);
        intent.putExtra("id", 100);
        intent.putExtra("c", i);
        startActivity(intent);

    }

    public void pushValues(final String comment) {
        DatabaseReference userDatabaseReference = FirebaseDatabase.getInstance().getReference("Users/" + FirebaseAuth.getInstance().getUid());
        userDatabaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    databaseReference.push().setValue(new Comment(dataSnapshot.child("Username").getValue().toString(), comment, dataSnapshot.child("Image").getValue().toString()));
                    commentEditText.setText("");
                    commentArrayList.clear();

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }

}
