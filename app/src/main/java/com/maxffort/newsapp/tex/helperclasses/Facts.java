package com.maxffort.newsapp.tex.helperclasses;

public class Facts {
    private String image;
    private String factId;
    int likeCount,commentCount;
    public Facts(){

    }

    public String getFactId() {
        return factId;
    }

    public void setFactId(String factId) {
        this.factId = factId;
    }

    public int getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(int likeCount) {
        this.likeCount = likeCount;
    }

    public int getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(int commentCount) {
        this.commentCount = commentCount;
    }

    public Facts(String image, String factId, int likeCount){
        this.image = image;
        this.factId = factId;
        this.likeCount = likeCount;

    }

    public Facts(String image){
        this.image = image;
    }

    public String getImageUrl() {
        return image;
    }

    public void setImageUrl(String imageUrl) {
        this.image = imageUrl;
    }
}
