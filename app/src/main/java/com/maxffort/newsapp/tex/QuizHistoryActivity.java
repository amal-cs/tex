package com.maxffort.newsapp.tex;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.maxffort.newsapp.tex.adapter.FeedAdapter;
import com.maxffort.newsapp.tex.adapter.QuizHistoryAdapter;
import com.maxffort.newsapp.tex.helperclasses.QuizHistory;

import java.util.ArrayList;

public class QuizHistoryActivity extends AppCompatActivity {

    private QuizHistoryAdapter quizHistoryAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private DatabaseReference databaseReference;
    private RecyclerView recyclerView;
    private static final String TAG = "QuizHistoryActivity";
    private ArrayList<QuizHistory> quizHistoryArrayList = new ArrayList<>();
    private TextView noquiz;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_history);
        recyclerView = findViewById(R.id.quizHistoryRecyclerView);
        noquiz = findViewById(R.id.noquiz);

        databaseReference = FirebaseDatabase.getInstance().getReference("Users/" + FirebaseAuth.getInstance().getUid() + "/Quiz");

        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                    quizHistoryArrayList.add(new QuizHistory(dataSnapshot1.getKey().toString(), dataSnapshot1.getValue().toString()));
                }
                if (quizHistoryArrayList.size() > 0) {
                    noquiz.setVisibility(View.INVISIBLE);
                }
                quizHistoryAdapter = new QuizHistoryAdapter(getApplicationContext(), quizHistoryArrayList);
                layoutManager = new LinearLayoutManager(getApplicationContext());
                recyclerView.setLayoutManager(layoutManager);
                recyclerView.setAdapter(quizHistoryAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }
}
