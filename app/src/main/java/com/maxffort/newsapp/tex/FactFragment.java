package com.maxffort.newsapp.tex;


import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSnapHelper;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.ads.consent.ConsentInformation;
import com.google.ads.consent.ConsentStatus;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.maxffort.newsapp.tex.adapter.FactAdapter;
import com.maxffort.newsapp.tex.helperclasses.Facts;
import com.maxffort.newsapp.tex.helperclasses.Feed;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;

public class FactFragment extends Fragment {

    private FactAdapter factAdapter;
    private LinearLayoutManager layoutManager;
    private ArrayList<Object> factsArrayList;
    private static final String TAG = "FactFragment";
    private RecyclerView recyclerView;
    private DatabaseReference databaseReference, dateDataReference;
    int i = 0, scrollPosition = 0;
    public ArrayList<String> datesArrayList = new ArrayList<>();
    boolean userScrolled = false;
    int position = 0;
    private AdView adView;
    private ProgressBar progressBar;
    Parcelable recylerViewState;
    int pastVisiblesItems, visibleItemCount, totalItemCount, likes;
    private static final int ITEMS_PER_AD = 3;


    public FactFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        View result = inflater.inflate(R.layout.fragment_fact, container, false);
        recyclerView = result.findViewById(R.id.factRecyclerView);

        progressBar = result.findViewById(R.id.pbLoadingFact);
        progressBar.setVisibility(ProgressBar.VISIBLE);
        recyclerView.setHasFixedSize(true);
        adView = result.findViewById(R.id.adView);
        Calendar calendar = Calendar.getInstance();

        if (getArguments() != null) {
            if (getArguments().getInt("id") == 100) {
                position = getArguments().getInt("scrollPosition");
            }
        }

        final String[] date = {Integer.toString(calendar.get(Calendar.DATE)) + "-" + Integer.toString(calendar.get(Calendar.MONTH) + 1) + "-" + Integer.toString(calendar.get(Calendar.YEAR))};


        ConsentInformation consentInformation = ConsentInformation.getInstance(getContext());
        Bundle extras = new Bundle();

        if (consentInformation.getConsentStatus().equals(ConsentStatus.NON_PERSONALIZED)) {
            extras.putString("npa", "1");
            AdRequest adRequest = new AdRequest.Builder()
                    .addNetworkExtrasBundle(AdMobAdapter.class, extras)
                    .build();
            adView.loadAd(adRequest);

        } else {
            AdRequest adRequest = new AdRequest.Builder().build();
            adView.loadAd(adRequest);
        }

        factsArrayList = new ArrayList<>();


        dateDataReference = FirebaseDatabase.getInstance().getReference("Facts");
        dateDataReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                    datesArrayList.add(dataSnapshot1.getKey());
                }
                Collections.reverse(datesArrayList);
                Log.d(TAG, "onDataChange: date" + datesArrayList);

                if (i == 0) {

                    loadData(datesArrayList.remove(0));
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    userScrolled = true;
                }
            }



            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                visibleItemCount = layoutManager.getChildCount();
                totalItemCount = layoutManager.getItemCount();
                pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();

                if (userScrolled && (visibleItemCount + pastVisiblesItems) == totalItemCount) {
                    userScrolled = false;

                    if ( datesArrayList.size() > 0) {

                        loadData(datesArrayList.remove(0));
//                        scrollPosition = scrollPosition + feedArrayList.size();
                        Log.d(TAG, "onScrolled: Scroll " + scrollPosition);
                    }
//                    } else
//                        Toast.makeText(getContext(), "End of facts", Toast.LENGTH_SHORT).show();
//                    getDate();


                }

            }
        });



        return result;
    }

    private void loadData(final String date) {
        scrollPosition = factsArrayList.size();
        databaseReference = FirebaseDatabase.getInstance().getReference("Facts/" + date);
        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                    Log.d(TAG, "onDataChange: image test " + dataSnapshot1);
                    if (dataSnapshot1.child("Image").exists()) {

                        if (dataSnapshot1.child("Likes").getValue() == null)
                            likes = 0;
                        else
                            likes = (int) dataSnapshot1.child("Likes").getChildrenCount();
                        factsArrayList.add(new Facts(dataSnapshot1.child("Image").getValue().toString(), date + "/" + dataSnapshot1.getKey(), likes));
                    }
                }

                Collections.sort(factsArrayList, new Comparator<Object>() {
                    @Override
                    public int compare(Object o1, Object o2) {
                        if (o1 instanceof Feed && o2 instanceof Feed) {
                            Facts f1 = (Facts) o1;
                            Facts f2 = (Facts) o2;
                            return f2.getFactId().compareTo(f1.getFactId());
                        }else
                            return 0;
                    }


                });


                SnapHelper snapHelper = new PagerSnapHelper();


                if (position == 0) {
                    factAdapter = new FactAdapter(getContext(), factsArrayList);
                    layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL,false);
                    recyclerView.setLayoutManager(layoutManager);
                    if (recyclerView.getOnFlingListener() == null)
                        snapHelper.attachToRecyclerView(recyclerView);
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    recyclerView.setAdapter(factAdapter);
                    recyclerView.scrollToPosition(scrollPosition);


                    progressBar.setVisibility(ProgressBar.INVISIBLE);
//                    i++;

                } else {
                    factAdapter = new FactAdapter(getContext(), factsArrayList);
                    layoutManager = new LinearLayoutManager(getContext(),LinearLayoutManager.HORIZONTAL,false);
                    recyclerView.setLayoutManager(layoutManager);
                    if (recyclerView.getOnFlingListener() == null)
                        snapHelper.attachToRecyclerView(recyclerView);
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    recyclerView.setAdapter(factAdapter);
                    recyclerView.scrollToPosition(position);
                    recyclerView.setOnFlingListener(null);

                    progressBar.setVisibility(ProgressBar.INVISIBLE);
//                    i++;
                    position = 0;
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

}
