package com.maxffort.newsapp.tex.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.maxffort.newsapp.tex.R;
import com.maxffort.newsapp.tex.helperclasses.FactComment;

import java.util.ArrayList;

public class FactCommentAdapter extends RecyclerView.Adapter<FactCommentAdapter.ViewHolder> {

    private ArrayList<FactComment> factCommentArrayList;
    private Context context;

    public FactCommentAdapter(Context context, ArrayList<FactComment> factCommentArrayList){
        this.context = context;
        this.factCommentArrayList = factCommentArrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.fact_comment_rows,viewGroup,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        FactComment factComment = factCommentArrayList.get(i);
        viewHolder.uname.setText(factComment.getUname());
        viewHolder.ucomment.setText(factComment.getUcomment());
        Glide.with(context).load(factComment.getUimgurl()).into(viewHolder.uImageView);
    }

    @Override
    public int getItemCount() {
        return factCommentArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private ImageView uImageView;
        private TextView uname, ucomment;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            uImageView = itemView.findViewById(R.id.fUserImage);
            uname = itemView.findViewById(R.id.fUserName);
            ucomment = itemView.findViewById(R.id.fComment);
        }
    }
}
