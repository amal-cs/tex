package com.maxffort.newsapp.tex.adapter;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.bumptech.glide.Glide;
import com.google.ads.consent.ConsentInformation;
import com.google.ads.consent.ConsentStatus;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.maxffort.newsapp.tex.CommentActivity;
import com.maxffort.newsapp.tex.DescribeActivity;
import com.maxffort.newsapp.tex.R;
import com.maxffort.newsapp.tex.helperclasses.Feed;

import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import static com.maxffort.newsapp.tex.MenuActivity.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE;
import static com.maxffort.newsapp.tex.MenuActivity.PERMISSIONS_FLAG;

public class FeedAdapter extends RecyclerView.Adapter {

    private final int ITEM_FEED = 1;
    private final int ITEM_AD = 0;
    private ArrayList<Object> feedArrayList;
    private DatabaseReference likeDatabaseReference, bookmarkDatabaseReference, commentDatabaseReference;
    private static final String TAG = "FeedAdapter";
    private boolean likeStatus;
    private Context context;

    public FeedAdapter(Context context, ArrayList<Object> feedArrayList) {

        this.feedArrayList = feedArrayList;
        this.context = context;

    }

//    @NonNull
//    @Override
//    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
//
//        switch (i){
//            case ITEM_AD:
//                View bannerAd = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.feed_list_ad_rows,viewGroup,false);
//                return new AdViewHolder(bannerAd);
//
//            case ITEM_FEED:
//                View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.feed_list_rows, viewGroup, false);
//                return new FeedViewHolder(view);
//        }
//
//
//
//    }


//    @Override
//    public void onBindViewHolder(@NonNull final FeedViewHolder feedViewHolder, final int i) {
//
//
//    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        switch (i) {
            case ITEM_AD:
                View bannerAd = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.feed_list_ad_rows, viewGroup, false);
                return new AdViewHolder(bannerAd);

            case ITEM_FEED:

            default:
                View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.feed_list_rows, viewGroup, false);
                return new FeedViewHolder(view);

        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int i) {

        int viewType = getItemViewType(i);

        switch (viewType) {
            case ITEM_FEED:
                Log.d(TAG, "onBindViewHolder: feed" + i);
                Log.d(TAG, "onBindViewHolder: Size of array " + feedArrayList.size());
                final FeedViewHolder feedViewHolder = (FeedViewHolder) viewHolder;
                final Feed feed = (Feed) feedArrayList.get(i);
                ((FeedViewHolder) viewHolder).headingTextView.setText(feed.getHeading());
                Glide.with(context).load(feed.getImageUrl()).into(feedViewHolder.newsImageView);
                feedViewHolder.fLike.setText(String.valueOf(feed.getLikeCount()));
//                feedViewHolder.feedDate.setText(String.valueOf(feed.getDate()));
                feedViewHolder.descriptionTextView.setText(String.valueOf(feed.getDescription()));
                isLiked(feed.getNewsId(), feedViewHolder);
                isBookmarked(feed.getNewsId(), feedViewHolder);
                findTimeStamp(feed.getTimeStamp(), feedViewHolder, feed);

                feedViewHolder.fShare.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (ContextCompat.checkSelfPermission(context,
                                Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                            Toast.makeText(context, "Give Storage Permission", Toast.LENGTH_SHORT).show();
                        } else {

                            Uri imageUri = null;
                            try {
                                feedViewHolder.newsImageView.buildDrawingCache();
                                Bitmap bitmap = feedViewHolder.newsImageView.getDrawingCache();
                                imageUri = Uri.parse(MediaStore.Images.Media.insertImage(context.getContentResolver(), bitmap, "title", "description"));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            Intent shareIntent = new Intent();
                            shareIntent.setAction(Intent.ACTION_SEND);
                            shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            shareIntent.putExtra(Intent.EXTRA_STREAM, imageUri);
                            shareIntent.putExtra(Intent.EXTRA_TEXT, feed.getHeading() + "\nTo know more : www.maxnews.live/newPage.html?result=News/" + feed.getNewsId() + "/Data");
                            shareIntent.setType("image/*");
                            context.startActivity(Intent.createChooser(shareIntent, "Share Via"));
                        }
                    }
                });

                feedViewHolder.fComment.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, CommentActivity.class);
                        intent.putExtra("NewsId", feed.getNewsId());
                        intent.putExtra("position", i);
                        context.startActivity(intent);

                        notifyDataSetChanged();
                    }
                });

                feedViewHolder.constraintLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent = new Intent(context, DescribeActivity.class);
                        intent.putExtra("SessionId", 300);
                        intent.putExtra("newsLink", feed.getLink());
                        intent.putExtra("newsImageId", feed.getImageUrl());
                        intent.putExtra("newsImageHeading", feed.getHeading());
                        intent.putExtra("newsImageDescription", feed.getDescription());
                        context.startActivity(intent);

                    }
                });


                feedViewHolder.fBookmark.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        bookmarkDatabaseReference = FirebaseDatabase.getInstance().getReference("Users/" + FirebaseAuth.getInstance().getUid() +
                                "/Bookmarks");
                        bookmarkDatabaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                String newNewsId = feed.getNewsId().replace('/', '@');

                                if (dataSnapshot.child(newNewsId).exists()) {

                                    bookmarkDatabaseReference.child(newNewsId).removeValue();
                                    feedViewHolder.fBookmark.setImageResource(R.drawable.ic_bookmark_border_black_24dp);
                                } else {
                                    bookmarkDatabaseReference.child(newNewsId).setValue(1);
                                    feedViewHolder.fBookmark.setImageResource(R.drawable.ic_bookmark_black_24dp);
                                }

                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });

                    }
                });

                //Comment
                feedViewHolder.fLikeImageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        likeDatabaseReference = FirebaseDatabase.getInstance().getReference("News/" + feed.getNewsId() + "/Likes");
                        likeDatabaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                if (FirebaseAuth.getInstance().getUid() != null)
                                    if (dataSnapshot.child(FirebaseAuth.getInstance().getUid()).exists()) {
                                        if (Integer.parseInt(feedViewHolder.fLike.getText().toString()) - 1 > 0) {
                                            feedViewHolder.fLikeImageView.setImageResource(R.drawable.ic_favorite_border_black_24dp);
                                            likeDatabaseReference.child(FirebaseAuth.getInstance().getUid()).removeValue();
                                            feedViewHolder.fLike.setText(String.valueOf(Integer.parseInt(feedViewHolder.fLike.getText().toString()) - 1));
                                        }
                                    } else {
                                        feedViewHolder.fLikeImageView.setImageResource(R.drawable.ic_favorite_black_24dp);
                                        likeDatabaseReference.child(FirebaseAuth.getInstance().getUid()).setValue(1);
                                        feedViewHolder.fLike.setText(String.valueOf(Integer.parseInt(feedViewHolder.fLike.getText().toString()) + 1));
                                    }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
                    }
                });


                break;


            case ITEM_AD:

            default:
                Log.d(TAG, "onBindViewHolder: ad " + i);
                ConsentInformation consentInformation = ConsentInformation.getInstance(context);
                Bundle extras = new Bundle();
                final AdViewHolder adViewHolder = (AdViewHolder) viewHolder;

                if (consentInformation.getConsentStatus().equals(ConsentStatus.NON_PERSONALIZED)) {
                    extras.putString("npa", "1");
                    AdRequest adRequest = new AdRequest.Builder()
                            .addNetworkExtrasBundle(AdMobAdapter.class, extras)
                            .build();
                    adViewHolder.adView.loadAd(adRequest);

                } else {
                    AdRequest adRequest = new AdRequest.Builder().build();
                    adViewHolder.adView.loadAd(adRequest);
                }


//                AdRequest adRequest = new AdRequest.Builder().build();


//                AdViewHolder adViewHolder = (AdViewHolder) viewHolder;
//                AdView adView = (AdView) feedArrayList.get(i);
//                ViewGroup viewGroup = (ViewGroup) adViewHolder.itemView;
//                if (adView.getChildCount() > 0){
//                    adView.removeAllViews();
//                }
//                if (adView.getParent() != null){
//                    ((ViewGroup ) adView.getParent()).removeView(adView);
//                }
//                viewGroup.addView(adView);

//                viewHolder.


        }


    }

    private void findTimeStamp(String timeStamp, FeedViewHolder feedViewHolder, Feed feed) {
        if (timeStamp.equals(""))
            feedViewHolder.feedDate.setText(String.valueOf(feed.getDate()));

        else {
            try {
                Log.d(TAG, "findTimeStamp: " + timeStamp);
                Calendar c = Calendar.getInstance();
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMM dd HH:mm:ss yyyy", Locale.US);
                Date date1 = simpleDateFormat.parse(timeStamp);
                String tempDate = simpleDateFormat.format(c.getTime());
                Date date2 = simpleDateFormat.parse(tempDate);

                long mills = date2.getTime() - date1.getTime();
                int hours = (int) mills / (1000 * 60 * 60);
                int mins = (int) (mills / (1000 * 60)) % 60;

                hours = (hours < 0) ? -hours : hours;
                mins = (mins < 0) ? -mins : mins;
                if (hours > 24)
                    feedViewHolder.feedDate.setText(String.valueOf(feed.getDate()));
                else if (hours < 1)
                    feedViewHolder.feedDate.setText(String.valueOf(mins) + " minutes ago");
                else
                    feedViewHolder.feedDate.setText(String.valueOf(hours) + " hours ago");
                String diff = hours + ":" + mins;
                Log.d(TAG, "onCreate: diff" + diff);
            } catch (Exception e) {

                Log.d(TAG, "onCreate: exception " + e);
                e.printStackTrace();
            }

        }

    }


    @Override
    public int getItemViewType(int position) {

        if (feedArrayList.get(position) == null)
            return ITEM_AD;
        else
            return ITEM_FEED;

//        if (feedArrayList.get(position) instanceof AdView)
//            return ITEM_AD;
//        else
//            return ITEM_FEED;

//        if (position % 4 == 0 && position != 0)
//            return ITEM_AD;
//        else
//            return ITEM_FEED;
    }

    @Override
    public int getItemCount() {
        return feedArrayList.size();
    }


    public class AdViewHolder extends RecyclerView.ViewHolder {

        public AdView adView;

        public AdViewHolder(@NonNull View itemView) {
            super(itemView);

            adView = itemView.findViewById(R.id.adView);
        }
    }

    public class FeedViewHolder extends RecyclerView.ViewHolder {

        private TextView headingTextView;
        private TextView feedDate;
        private TextView descriptionTextView;
        private ImageView newsImageView, fBookmark, fLikeImageView, fShare, fComment;
        private TextView fLike;
        private LinearLayout likeLayout;
        private ConstraintLayout constraintLayout;
        private CardView cardView;

        public FeedViewHolder(@NonNull View itemView) {
            super(itemView);


            descriptionTextView = itemView.findViewById(R.id.fDescriptionTextView);
            fComment = itemView.findViewById(R.id.fComment);
            constraintLayout = itemView.findViewById(R.id.feedConstraintLayout);
//            cardView = itemView.findViewById(R.id.feedCardId);
            fLikeImageView = itemView.findViewById(R.id.fLikeImageView);
            headingTextView = itemView.findViewById(R.id.fTitleTextView);
            newsImageView = itemView.findViewById(R.id.fNewsImage);
            newsImageView.setDrawingCacheEnabled(true);
            fBookmark = itemView.findViewById(R.id.fBookmark);
//            fComment = itemView.findViewById(R.id.fComment);
            fLike = itemView.findViewById(R.id.fLike);
//            fShare = itemView.findViewById(R.id.fShare);
//            likeLayout = itemView.findViewById(R.id.likeLayout);
            feedDate = itemView.findViewById(R.id.newsDate);
            fShare = itemView.findViewById(R.id.fShare);

        }
    }

    public void isBookmarked(String newsId, final FeedViewHolder viewHolder) {
        final String newNewsId = newsId.replace('/', '@');
        bookmarkDatabaseReference = FirebaseDatabase.getInstance().getReference("Users/" + FirebaseAuth.getInstance().getUid() + "/Bookmarks/");
        bookmarkDatabaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                if (dataSnapshot.hasChild(newNewsId)) {
                    Log.d(TAG, "onDataChange: FeedFr has ");
                    viewHolder.fBookmark.setImageResource(R.drawable.ic_bookmark_black_24dp);
                } else {
                    Log.d(TAG, "onDataChange: FeedFr doesn't has ");
                    viewHolder.fBookmark.setImageResource(R.drawable.ic_bookmark_border_black_24dp);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }


    public void isLiked(String newsId, final FeedViewHolder viewHolder) {
        likeDatabaseReference = FirebaseDatabase.getInstance().getReference("News/" + newsId + "/Likes");
        final boolean[] status = new boolean[1];
        likeDatabaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (FirebaseAuth.getInstance().getUid() != null)
                    if (dataSnapshot.hasChild(FirebaseAuth.getInstance().getUid())) {
                        viewHolder.fLikeImageView.setImageResource(R.drawable.ic_favorite_black_24dp);
                        Log.d(TAG, "onDataChange: if " + dataSnapshot.hasChild(FirebaseAuth.getInstance().getUid()));
                        status[0] = true;
                    } else {
                        viewHolder.fLikeImageView.setImageResource(R.drawable.ic_favorite_border_black_24dp);
                        Log.d(TAG, "onDataChange: else " + dataSnapshot.hasChild(FirebaseAuth.getInstance().getUid()));
                        status[0] = false;
                    }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

}
