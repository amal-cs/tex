package com.maxffort.newsapp.tex;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.ads.consent.ConsentForm;
import com.google.ads.consent.ConsentFormListener;
import com.google.ads.consent.ConsentInfoUpdateListener;
import com.google.ads.consent.ConsentInformation;
import com.google.ads.consent.ConsentStatus;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.maxffort.newsapp.tex.adapter.FeedAdapter;
import com.maxffort.newsapp.tex.helperclasses.Feed;
import com.maxffort.newsapp.tex.helperclasses.Leaderboard;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;


public class FeedFragment extends Fragment {

    private FeedAdapter feedAdapter;
    private RecyclerView feedRecyclerView;
    private ConsentForm consentForm;
    private RelativeLayout relativeLayout;
    private String timeStamp;
    private DatabaseReference dataReference, dateDataReference,updateDatabaseReference;
    private static final String TAG = "FeedFragment";
    public int likes, i = 0, scrollPosition = 0, comment;
    private ProgressBar progressBar;
    public ArrayList<Object> feedArrayList = new ArrayList<>();
    public ArrayList<String> datesArrayList = new ArrayList<>();
    public static ArrayList<String> imageUrl = new ArrayList<>();
    public static ArrayList<String> headings = new ArrayList<>();
    public static ArrayList<String> id = new ArrayList<>();
    public static ArrayList<String> description = new ArrayList<>();
    public static ArrayList<Long> likeCount = new ArrayList<>();
    public LinearLayoutManager layoutManager;
    private int prevArraylistSize = 0, position = 0;
    boolean userScrolled = false;

    private static final int ITEMS_PER_AD = 3;
    private static final String BANNER_AD_ID = "ca-app-pub-3940256099942544~3347511713";
    int pastVisiblesItems, visibleItemCount, totalItemCount;

    private Context context;

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        View result = inflater.inflate(R.layout.fragment_feed, container, false);
//        relativeLayout = result.findViewById(R.id.feedFragmentRelativeLayout);
        feedRecyclerView = result.findViewById(R.id.feedRecyclerView);
        progressBar = result.findViewById(R.id.pbLoadingFeed);
        progressBar.setVisibility(ProgressBar.VISIBLE);

        Calendar calendar = Calendar.getInstance();


        if (getArguments() != null) {
            if (getArguments().getInt("id") == 200) {
                position = getArguments().getInt("scrollPosition");
//                Toast.makeText(getContext(), "Position is " + position, Toast.LENGTH_SHORT).show();
            }
        }


        final String[] date = {Integer.toString(calendar.get(Calendar.DATE)) + "-" + Integer.toString(calendar.get(Calendar.MONTH) + 1) + "-" + Integer.toString(calendar.get(Calendar.YEAR))};

//        MobileAds.initialize(getContext(), "ca-app-pub-2174236293887146/7905114177");
//
        ConsentInformation consentInformation = ConsentInformation.getInstance(getContext());

        if (consentInformation.isRequestLocationInEeaOrUnknown()) {


            consentInformation.requestConsentInfoUpdate(new String[]{"pub-2174236293887146"}, new ConsentInfoUpdateListener() {
                @Override
                public void onConsentInfoUpdated(ConsentStatus consentStatus) {


                    if (consentStatus == ConsentStatus.UNKNOWN) {

                        displayConsentForm();
                    }
                }

                @Override
                public void onFailedToUpdateConsentInfo(String reason) {
                    Log.d(TAG, "onFailedToUpdateConsentInfo: " + reason);
                }
            });

        }


        updateDatabaseReference = FirebaseDatabase.getInstance().getReference("update");
        updateDatabaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                int update = Integer.parseInt(dataSnapshot.getValue().toString());

                if (update != 0) {
                    PackageManager packageManager = getContext().getPackageManager();
                    try {
                        PackageInfo packageInfo = packageManager.getPackageInfo(getContext().getPackageName(), PackageManager.GET_ACTIVITIES);
                        if (packageInfo.versionCode < update) {
                            new AlertDialog.Builder(getContext())
                                    .setTitle("Please Update").setMessage("Update App to the latest version")
                                    .setCancelable(false)
                                    .setPositiveButton("Done", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            final String appPackageName = getContext().getPackageName();
                                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                                        }
                                    })
                                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            getActivity().finishAffinity();
                                        }
                                    })

                                    .show();
                        }
//                        Toast.makeText(MenuActivity.this, packageInfo.versionCode+"", Toast.LENGTH_SHORT).show();
                    } catch (PackageManager.NameNotFoundException e) {
                        e.printStackTrace();
                    }
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        dateDataReference = FirebaseDatabase.getInstance().getReference("News");
        dateDataReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {

                    Log.d(TAG, "onDataChange: datesArrayList inside");
                    datesArrayList.add(dataSnapshot1.getKey());
//                    size[0] = dataSnapshot.getChildrenCount();
                }
                Collections.reverse(datesArrayList);
//                Log.d(TAG, "onDataChange: date" + datesArrayList);

                if (i == 0) {
//                    Toast.makeText(getContext(), "" + datesArrayList.get(i), Toast.LENGTH_SHORT).show();
                    loadData(datesArrayList.remove(0));
                }
                Log.d(TAG, "onDataChange: datelist " + datesArrayList);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


//        GradientDrawable gradientDrawable = new GradientDrawable();
//        gradientDrawable.setColors(new int[]{
//                Color.rgb(212,117,110),Color.rgb(24,57,163)
//        });
//
//        gradientDrawable.setGradientType(GradientDrawable.LINEAR_GRADIENT);
//        gradientDrawable.setShape(GradientDrawable.RECTANGLE);
//
//        relativeLayout.setBackground(gradientDrawable);


        feedRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    userScrolled = true;
                }
            }


            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                visibleItemCount = layoutManager.getChildCount();
                totalItemCount = layoutManager.getItemCount();
                pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();

                if (userScrolled && (visibleItemCount + pastVisiblesItems) == totalItemCount) {
                    userScrolled = false;

                    if (datesArrayList.size() > 0) {
                        Log.d(TAG, "onScrolled: Iteration " + datesArrayList.get(i));
                        loadData(datesArrayList.remove(0));
//                        scrollPosition = scrollPosition + feedArrayList.size();
                        Log.d(TAG, "onScrolled: Scroll " + scrollPosition);
                    } else {
                        Toast.makeText(getContext(), "End of news", Toast.LENGTH_SHORT).show();
                    }

//                    getDate();

//                    Toast.makeText(getContext(), "User scrolled", Toast.LENGTH_SHORT).show();
                }
            }
        });
        return result;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
    }

    public void loadData(final String date) {
        scrollPosition = feedArrayList.size();
        Log.d(TAG, "loadData: datesArrayList " + datesArrayList);
        dataReference = FirebaseDatabase.getInstance().getReference("News/" + date);
        dataReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                    if (dataSnapshot1.child("Data").child("Heading").exists() && dataSnapshot1.child("Data").child("Image").exists()
                            && dataSnapshot1.child("Data").child("Description").exists() && dataSnapshot1.child("Data").child("Link").exists()) {


                        if (dataSnapshot1.child("Likes").getValue() == null)
                            likes = 0;
                        else
                            likes = (int) dataSnapshot1.child("Likes").getChildrenCount();
                        if (dataSnapshot1.child("Comments").getValue() == null)
                            comment = 0;
                        else
                            comment = (int) dataSnapshot1.child("Comments").getChildrenCount();

                        if (dataSnapshot1.child("Data").child("Time").getValue() == null)
                            timeStamp = "";
                         else
                             timeStamp = dataSnapshot1.child("Data").child("Time").getValue().toString();

                        Log.d(TAG, "onDataChange: get" + dataSnapshot1.child("Data").child("Link").getValue());
                        Feed feed = new Feed(dataSnapshot1.child("Data").child("Heading").getValue().toString(), dataSnapshot1.child("Data").child("Image").getValue().toString(),
                                comment, likes, date + "/" + dataSnapshot1.getKey(), dataSnapshot1.child("Data").child("Description").getValue().toString(),
                                dataSnapshot.getKey(), dataSnapshot1.child("Data").child("Link").getValue().toString(),timeStamp);
                        feedArrayList.add(feed);

                    }

                }

                Collections.sort(feedArrayList, new Comparator<Object>() {
                    @Override
                    public int compare(Object o1, Object o2) {
                        if (o1 instanceof Feed && o2 instanceof Feed){
                            Feed f1 = (Feed) o1;
                            Feed f2 = (Feed) o2;
                            return f2.getNewsId().compareTo(f1.getNewsId());
                        }
                        else return 0;

                    }


                });

                for (int i = 2; i < feedArrayList.size(); i += ITEMS_PER_AD) {
//                    final AdView adView = new AdView(getContext());
//                    adView.setAdSize(AdSize.BANNER);
//                    adView.setAdUnitId("ca-app-pub-3940256099942544/6300978111");
//                    feedArrayList.add(i,adView);
                    if (feedArrayList.get(i) != null)
                        feedArrayList.add(i, null);
                }

//                prevArraylistSize = feedArrayList.size();

//                loadBannerAd();
                if (position == 0) {
                    feedAdapter = new FeedAdapter(getContext(), feedArrayList);
                    layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
                    feedRecyclerView.setLayoutManager(layoutManager);
                    feedRecyclerView.setAdapter(feedAdapter);
                    feedRecyclerView.scrollToPosition(pastVisiblesItems);
                    progressBar.setVisibility(ProgressBar.INVISIBLE);
//                    i++;
                } else {
                    feedAdapter = new FeedAdapter(getContext(), feedArrayList);
                    layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
                    feedRecyclerView.setLayoutManager(layoutManager);
                    feedRecyclerView.setItemAnimator(new DefaultItemAnimator());
                    feedRecyclerView.setAdapter(feedAdapter);
                    feedRecyclerView.scrollToPosition(position);
                    progressBar.setVisibility(ProgressBar.INVISIBLE);
//                    i++;
                    position = 0;
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }


    private void loadBannerAd() {
        for (int i = 0; i < feedArrayList.size(); i++) {
            Object item = feedArrayList.get(i);
            if (item instanceof AdView) {
                final AdView adView = (AdView) item;
                adView.loadAd(new AdRequest.Builder().build());
            }
        }
    }

    //
    public void moveTo(int i) {
        Log.d(TAG, "moveTo: " + i);
        if (feedAdapter != null && feedRecyclerView != null) {
            feedAdapter.notifyDataSetChanged();


            feedRecyclerView.scrollToPosition(i);
        }


    }


    private void displayConsentForm() {
        consentForm = new ConsentForm.Builder(getContext(), getAppsPrivacyPolicy()).withListener(new ConsentFormListener() {
            @Override
            public void onConsentFormLoaded() {
                super.onConsentFormLoaded();
                consentForm.show();
                Log.d(TAG, "onConsentFormLoaded: ");
            }

            @Override
            public void onConsentFormError(String reason) {
                super.onConsentFormError(reason);
                Log.d(TAG, "onConsentFormError: " + reason);
            }

            @Override
            public void onConsentFormOpened() {
                super.onConsentFormOpened();
            }

            @Override
            public void onConsentFormClosed(ConsentStatus consentStatus, Boolean userPrefersAdFree) {
                super.onConsentFormClosed(consentStatus, userPrefersAdFree);
            }
        }).withPersonalizedAdsOption()
                .withNonPersonalizedAdsOption()
                .build();
        consentForm.load();
//        Toast.makeText(this, "final", Toast.LENGTH_SHORT).show();
    }

    private URL getAppsPrivacyPolicy() {
        URL mUrl = null;
        try {
            mUrl = new URL("https://lanciar.com/policy.pdf");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return mUrl;
    }


}
