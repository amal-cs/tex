package com.maxffort.newsapp.tex.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.maxffort.newsapp.tex.DescribeActivity;
import com.maxffort.newsapp.tex.R;
import com.maxffort.newsapp.tex.helperclasses.Bookmark;

import java.util.ArrayList;

public class BookmarkAdapter extends RecyclerView.Adapter<BookmarkAdapter.BookmarkViewHolder> {

    private ArrayList<Bookmark> bookmarkArrayList;
    private Context context;
    private static final String TAG = "BookmarkAdapter";

    public BookmarkAdapter(Context context, ArrayList<Bookmark> bookmarkArrayList){
        this.context = context;
        this.bookmarkArrayList = bookmarkArrayList;
    }
    @NonNull
    @Override
    public BookmarkViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.bookmark_cards,viewGroup,false);
        return new BookmarkAdapter.BookmarkViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final BookmarkViewHolder bookmarkViewHolder, final int i) {
        final Bookmark bookmark = bookmarkArrayList.get(i);

        bookmarkViewHolder.bookmarkCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, DescribeActivity.class);
                intent.putExtra("SessionId",200);
                intent.putExtra("newsImageId",bookmark.getImage());
                intent.putExtra("newsImageHeading",bookmark.getHeading());
                intent.putExtra("newsImageDescription",bookmark.getDescription());
                context.startActivity(intent);

            }
        });

        bookmarkViewHolder.bookmarkIndicator.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeBookmark(bookmark.getNewsId());
                bookmarkArrayList.remove(i);
                notifyItemRemoved(i);
                notifyDataSetChanged();
//                notifyItemRangeRemoved(i,bookmarkArrayList.size());


            }
        });

        bookmarkViewHolder.bookmarkHeading.setText(bookmark.getHeading());
        Glide.with(context).load(bookmark.getImage()).into(bookmarkViewHolder.bookmarkImage);
    }

    @Override
    public int getItemCount() {
        return bookmarkArrayList.size();
    }


    class BookmarkViewHolder extends RecyclerView.ViewHolder{

        ImageView bookmarkImage,bookmarkIndicator;
        CardView bookmarkCardView;
        TextView bookmarkHeading,nocontentTextView;
        BookmarkViewHolder(@NonNull View itemView) {
            super(itemView);
            bookmarkIndicator = itemView.findViewById(R.id.bookmarkIndicator);
            bookmarkCardView = itemView.findViewById(R.id.bookmarkcard);
            bookmarkImage = itemView.findViewById(R.id.bookmarkImage);
            bookmarkHeading = itemView.findViewById(R.id.bookmarkHeading);

        }
    }

    private void removeBookmark(String newId){
        DatabaseReference bookmarkDatabaseReference = FirebaseDatabase.getInstance().getReference("Users/" + FirebaseAuth.getInstance().getUid()
                + "/Bookmarks/");
        bookmarkDatabaseReference.child(newId).removeValue();
    }

}


