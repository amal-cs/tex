package com.maxffort.newsapp.tex.helperclasses;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.maxffort.newsapp.tex.QuizActivity;
import com.maxffort.newsapp.tex.R;

public class QuizDialogueClass extends Dialog {

    private Activity activity;
    private int status,weekOfYear;
    private String hourRemain,minRemain;
    private static final String TAG = "QuizDialogueClass";
    private TextView heading, subHeading, quizDialogueRules1,quizDialogueRules2,quizDialogueRules3,quizDialogueRules4,quizDialogueRules5, startButton, cancelButton;

    public QuizDialogueClass(Activity activity, int status,int weekOfYear) {
        super(activity);
        this.activity = activity;
        this.status = status;
        this.weekOfYear = weekOfYear;
    }

    public QuizDialogueClass(Activity activity, int status,String hourRemain,String minRemain) {
        super(activity);
        this.activity = activity;
        this.status = status;
        this.hourRemain = hourRemain;
        this.minRemain = minRemain;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.start_quiz_dialogue);

        heading = findViewById(R.id.quizDialogueHeading);
        subHeading = findViewById(R.id.quizDialogueSubHeading);
        quizDialogueRules1 = findViewById(R.id.quizDialogueRules1);
        quizDialogueRules2 = findViewById(R.id.quizDialogueRules2);
        quizDialogueRules3 = findViewById(R.id.quizDialogueRules3);
        quizDialogueRules4 = findViewById(R.id.quizDialogueRules4);
        quizDialogueRules5 = findViewById(R.id.quizDialogueRules5);
        startButton = findViewById(R.id.quizDialogueStartTextView);
        cancelButton = findViewById(R.id.quizDialogueCancelTextView);

        if (status == 1) {
            startButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getContext(), QuizActivity.class);
                    intent.putExtra("weekOfYear",weekOfYear);
                    Log.d(TAG, "onClick: Week of year dialoue class "+ weekOfYear);
                    activity.startActivity(intent);
                    activity.finish();
                }
            });

            cancelButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                }
            });

        }else{
            startButton.setVisibility(View.INVISIBLE);
            quizDialogueRules1.setVisibility(View.GONE);
            quizDialogueRules2.setVisibility(View.GONE);
            quizDialogueRules3.setVisibility(View.GONE);
            quizDialogueRules4.setVisibility(View.GONE);
            quizDialogueRules5.setVisibility(View.GONE);
            heading.setText("Warning");
            subHeading.setText("You already Played Today.\nYou can play after "+hourRemain+" hours and "+minRemain+" minutes.");
            cancelButton.setGravity(Gravity.END);

            cancelButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                }
            });
        }



    }

//    @Override
//    public void onClick(View v) {
////
////        switch (v.getId()) {
////            case R.id.quizDialogueStartTextView:
////
////                break;
////
////            case R.id.quizDialogueCancelTextView:
////
////                break;
////            default:
////                dismiss();
////        }
//
//    }
}
