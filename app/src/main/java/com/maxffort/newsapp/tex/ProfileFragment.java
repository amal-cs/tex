package com.maxffort.newsapp.tex;


import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.maxffort.newsapp.tex.adapter.BookmarkAdapter;
import com.maxffort.newsapp.tex.helperclasses.Bookmark;

import java.util.ArrayList;

import static android.support.constraint.Constraints.TAG;


public class ProfileFragment extends Fragment {


    private ArrayList<Bookmark> bookmarkArrayList;
    private TextView userNameTextView,emailTextView,nocontentTextView;
    private ImageView userImageView,settings;
    private BookmarkAdapter bookmarkAdapter;
    private CoordinatorLayout coordinatorLayout;
    private RecyclerView recyclerView;
    private FirebaseAuth firebaseAuth;
    private DatabaseReference databaseReference;
    private RecyclerView.LayoutManager layoutManager;
    private ImageView leaderBtnImage;
    private LinearLayout signOutLinearLayout,quizHistoryLinearLayout;
    private LinearLayout leaderBtnText;
    private FrameLayout profileFrameLayout;
    private LinearLayout nobookmarkcard,bookmarkvisible;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View result = inflater.inflate(R.layout.fragment_profile, container, false);
        recyclerView = result.findViewById(R.id.bookmarkRecyclerView);
        recyclerView.setHasFixedSize(true);
        nobookmarkcard = result.findViewById(R.id.nobookmarkcard);
        quizHistoryLinearLayout = result.findViewById(R.id.quizHistoryLinearLayout);
        bookmarkArrayList = new ArrayList<>();
        leaderBtnText = result.findViewById(R.id.settings);
        userImageView = result.findViewById(R.id.userImageView);
        userNameTextView = result.findViewById(R.id.userNameTextView);
        emailTextView = result.findViewById(R.id.emailTextView);
        signOutLinearLayout = result.findViewById(R.id.signOut);
        coordinatorLayout = result.findViewById(R.id.profileCoordinatorLayout);
        bookmarkvisible = result.findViewById(R.id.bookmarkvisible);



        GoogleSignInAccount acct = GoogleSignIn.getLastSignedInAccount(getContext());
        if (acct != null) {
            String personName = acct.getDisplayName();
            String personEmail = acct.getEmail();
            Uri personPhoto = acct.getPhotoUrl();

            userNameTextView.setText(personName);
            emailTextView.setText(personEmail);
            Glide.with(result.getContext()).load(personPhoto).into(userImageView);
        }

        quizHistoryLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(),QuizHistoryActivity.class);
                startActivity(intent);
            }
        });

        signOutLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAuth.getInstance().signOut();
                Intent intent = new Intent(getContext(),LoginActivity.class);
                startActivity(intent);
                getActivity().finish();

            }
        });

        leaderBtnText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(),Settings.class);
                startActivity(intent);
            }
        });



            databaseReference = FirebaseDatabase.getInstance().getReference("Users/" + firebaseAuth.getInstance().getUid() + "/Bookmarks");
            databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    for (final DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                        Log.d(TAG, "onDataChange: Testing data"+dataSnapshot1);
                        final String path = dataSnapshot1.getKey().replace('@','/');
                        DatabaseReference profileDatabaseReference = FirebaseDatabase.getInstance().getReference("News/" + path);
                        profileDatabaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                for (DataSnapshot dataSnapshot2 : dataSnapshot.getChildren()) {
                                    if (dataSnapshot2.getKey().equals("Data")) {
                                        Log.d(TAG, "onDataChange: deleted");
                                        bookmarkArrayList.add(new Bookmark(dataSnapshot2.child("Heading").getValue().toString(), dataSnapshot2.child("Image").getValue().toString(),
                                                dataSnapshot2.child("Description").getValue().toString(),dataSnapshot1.getKey()));
                                    }
                                }

                                if(bookmarkArrayList.size() == 0){
                                    Log.d(TAG, "onDataChange: Size of arraylist is "+bookmarkArrayList.size());
                                    nobookmarkcard.setVisibility(View.VISIBLE);
                                }
                                else {
                                    Log.d(TAG, "onDataChange: Size of arraylist is "+bookmarkArrayList.size());
                                    bookmarkvisible.setVisibility(View.VISIBLE);
                                    nobookmarkcard.setVisibility(View.GONE);
                                    bookmarkAdapter = new BookmarkAdapter(getContext(), bookmarkArrayList);
                                    layoutManager = new LinearLayoutManager(getContext());
                                    recyclerView.setLayoutManager(layoutManager);
                                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                                    recyclerView.setAdapter(bookmarkAdapter);
                                }

                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });

                    }
//                    Toast.makeText(getContext(), ""+bookmarkArrayList.size(), Toast.LENGTH_SHORT).show();
//                    if(bookmarkArrayList.size() == 0){
//
//                    }
//                    else{
//                        n
//                    }
                }



                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
            if (bookmarkArrayList.size()==0){
                nobookmarkcard.setVisibility(View.VISIBLE);
            }

        return result;
    }


}
