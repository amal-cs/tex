package com.maxffort.newsapp.tex;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.functions.FirebaseFunctions;
import com.google.firebase.functions.HttpsCallableResult;
import com.maxffort.newsapp.tex.adapter.LeaderboardAdapter;
import com.maxffort.newsapp.tex.helperclasses.Leaderboard;
import com.maxffort.newsapp.tex.helperclasses.QuizDialogueClass;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Hours;
import org.joda.time.Minutes;
import org.joda.time.Seconds;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;
import java.util.Scanner;

import javax.net.ssl.HttpsURLConnection;

import io.fabric.sdk.android.services.network.HttpRequest;

import static android.support.constraint.Constraints.TAG;


public class QuizFragment extends Fragment {


    private DatabaseReference quizDatabaseReference, leaderboardDatabaseReference, prizeDatabaseReference;
    private String checkFromTime, checkToTime;
    private FirebaseFunctions dateFirebaseFunctions;
    private boolean timerRunning;
    private ConstraintLayout constraintLayout;
    private RecyclerView leaderboardRecyclerView;
    private TextView countDownTextView;
    private boolean status;
    private String quizDate;
    private ProgressBar progressBar;
    private CountDownTimer countDownTimer;
    private Button startQuizButton;
    private ArrayList<Leaderboard> leaderboardArrayList = new ArrayList<>();
    private ConstraintLayout quizConstraintLayout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_quiz, container, false);

//        constraintLayout = view.findViewById(R.id.quizConstraintLayout);
        progressBar = view.findViewById(R.id.quizLoading);
        leaderboardRecyclerView = view.findViewById(R.id.leaderboardRecyclerView);
        quizConstraintLayout = view.findViewById(R.id.quizConstraintLayout);
        startQuizButton = view.findViewById(R.id.startQuizButton);
//        prizetextview = view.findViewById(R.id.prizetextview);
        dateFirebaseFunctions = FirebaseFunctions.getInstance();

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
        Date date = new Date();
        checkFromTime = simpleDateFormat.format(date);


        countDownTextView = view.findViewById(R.id.countDownTextView);
        quizDatabaseReference = FirebaseDatabase.getInstance().getReference("Quiz/Time");
//        prizeDatabaseReference = FirebaseDatabase.getInstance().getReference("Prize");
//        prizeDatabaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                prizetextview.setText(dataSnapshot.getValue().toString());
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError databaseError) {
//
//            }
//        });

        Calendar calendar = Calendar.getInstance();
        if (Calendar.getInstance().getFirstDayOfWeek() == 2 && Calendar.getInstance().get(Calendar.DAY_OF_WEEK) == 1) {
            int weekNum = calendar.get(Calendar.WEEK_OF_YEAR) + 1;
            leaderboardDatabaseReference = FirebaseDatabase.getInstance().getReference("Quiz/Leaderboard/" + weekNum);
        } else
            leaderboardDatabaseReference = FirebaseDatabase.getInstance().getReference("Quiz/Leaderboard/" + calendar.get(Calendar.WEEK_OF_YEAR));


//        Query query = leaderboardDatabaseReference.limitToLast(1);
//        Toast.makeText(getContext(), "" + Calendar.getInstance().getFirstDayOfWeek() + "day" + Calendar.getInstance().get(Calendar.DAY_OF_WEEK), Toast.LENGTH_SHORT).show();

        final Query leaderboardQuery = leaderboardDatabaseReference.orderByValue().limitToLast(10);
        leaderboardQuery.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.getChildrenCount() != 0) {
                    for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                        leaderboardArrayList.add(new Leaderboard(dataSnapshot1.getKey(), dataSnapshot1.getValue().toString()));
//                        if (leaderboardArrayList.size() == 10)
//                            break;

                    }

                    Collections.sort(leaderboardArrayList, new Comparator<Leaderboard>() {
                        @Override
                        public int compare(Leaderboard o1, Leaderboard o2) {
                            return Integer.compare(Integer.valueOf(o2.getTime()), Integer.valueOf(o1.getTime()));
                        }


                    });


                    LeaderboardAdapter leaderboardAdapter = new LeaderboardAdapter(getContext(), leaderboardArrayList);
                    RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
                    leaderboardRecyclerView.setLayoutManager(layoutManager);
                    leaderboardRecyclerView.setAdapter(leaderboardAdapter);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        startQuizButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                GetDate getDate = new GetDate();
                getDate.execute("https://us-central1-project-tex-8f4f2.cloudfunctions.net/getTime");

            }
        });

        return view;
    }


    public void canPlay(final String quizDate, final String hourRemain, final String minRemain) {

//        ServerValue.TIMESTAMP


//        final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
//        final Date date = new Date();

        DatabaseReference playedDatabaserReference = FirebaseDatabase.getInstance().getReference("Users/" + FirebaseAuth.getInstance().getUid()
                + "/Quiz");
        playedDatabaserReference.keepSynced(true);
        playedDatabaserReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (!dataSnapshot.child(quizDate).exists()) {
                    startQuiz(1, hourRemain, minRemain, quizDate);
                } else {
//                    Toast.makeText(getContext(), "You already played today", Toast.LENGTH_SHORT).show();
                    startQuiz(0, hourRemain, minRemain, quizDate);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    public void startQuiz(int status, String hourRemain, String minRemain, String date) {


        Date date1;
        int weekOfYear;
        Calendar calendar = Calendar.getInstance();
        try {
            date1 = new SimpleDateFormat("yyyy-MM-dd").parse(date);
            calendar.setTime(date1);
            weekOfYear = calendar.get(Calendar.WEEK_OF_YEAR);
            if (status == 1) {
                QuizDialogueClass quizDialogueClass = new QuizDialogueClass(getActivity(), status, weekOfYear);
                quizDialogueClass.show();
            } else {
                QuizDialogueClass quizDialogueClass = new QuizDialogueClass(getActivity(), status, hourRemain, minRemain);
                quizDialogueClass.show();
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }


    }


    public String getRequest(URL url) throws IOException {
        HttpsURLConnection httpsURLConnection = (HttpsURLConnection) url.openConnection();
        try {

            InputStream inputStream = httpsURLConnection.getInputStream();
            Scanner scanner = new Scanner(inputStream);
            scanner.useDelimiter("\\A");

            Boolean isData = scanner.hasNext();
            if (isData) {
                return scanner.next();
            } else {
                return null;
            }
        } finally {
            httpsURLConnection.disconnect();
        }

    }


    public URL urlBuilder(String query) {

        URL url = null;
        try {
            url = new URL("https://us-central1-project-tex-8f4f2.cloudfunctions.net/getTime");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return url;

    }


    public class GetDate extends AsyncTask<String, Void, String> {


        @Override
        protected String doInBackground(String... strings) {
            if (strings == null)
                return null;
            String string = null;
            try {
                URL url = urlBuilder(strings[0]);
                string = getRequest(url);
                //return string;

            } catch (Exception e) {
                e.printStackTrace();
            }
            return string;

        }

        @Override
        protected void onPreExecute() {
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            quizDate = s.replace('"', ' ').trim();

            String[] date = quizDate.split("T");
//            String[] quizTime = date[1].split(".");

            String timeStart = date[1].substring(0, 8);
            String timeStop = "24:00:00";

            Log.d(TAG, "onPostExecute: Server date "+s);

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss");

            Date date1 = null;
            Date date2 = null;

            try {

                date1 = simpleDateFormat.parse(timeStart);
                date2 = simpleDateFormat.parse(timeStop);

                DateTime dt1 = new DateTime(date1);
                DateTime dt2 = new DateTime(date2);

                String hourRemain = String.valueOf(Hours.hoursBetween(dt1, dt2).getHours() % 24);
                String minRemain = String.valueOf(Minutes.minutesBetween(dt1, dt2).getMinutes() % 60);

                Log.d(TAG, "onPostExecute: hour " + Hours.hoursBetween(dt1, dt2).getHours() % 24);
                Log.d(TAG, "onPostExecute: minutes " + Minutes.minutesBetween(dt1, dt2).getMinutes() % 60);

                progressBar.setVisibility(View.INVISIBLE);
                canPlay(date[0], hourRemain, minRemain);

            } catch (Exception e) {
                Log.e(TAG, "onPostExecute: " + e.getMessage());
            }


//            Log.d(TAG, "onPostExecute: Quiz date "+quizDate);


        }
    }


}
