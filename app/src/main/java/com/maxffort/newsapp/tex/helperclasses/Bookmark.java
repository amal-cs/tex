package com.maxffort.newsapp.tex.helperclasses;

public class Bookmark {
    public String heading,image,description,newsId;
    public Bookmark(){}

    public Bookmark(String heading, String image,String description,String newsId){
        this.heading = heading;
        this.newsId = newsId;
        this.image = image;
        this.description = description;
    }

    public String getNewsId() {
        return newsId;
    }

    public void setNewsId(String newsId) {
        this.newsId = newsId;
    }

    public void setHeading(String heading) {
        this.heading = heading;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getHeading() {
        return heading;
    }

    public String getImage() {
        return image;
    }
}
