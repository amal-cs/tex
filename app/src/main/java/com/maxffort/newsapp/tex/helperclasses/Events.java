package com.maxffort.newsapp.tex.helperclasses;

public class Events {

    private String EventDescription,EventImage,EventName,EventTime;

    public Events(){

    }
    public Events(String eventDescription, String eventImage, String eventName,String eventTime) {
        EventTime = eventTime;
        EventDescription = eventDescription;
        EventImage = eventImage;
        EventName = eventName;

    }

    public String getEventTime() {
        return EventTime;
    }


    public String getEventDescription() {
        return EventDescription;
    }

    public void setEventDescription(String eventDescription) {
        EventDescription = eventDescription;
    }

    public String getEventImage() {
        return EventImage;
    }

    public void setEventImage(String eventImage) {
        EventImage = eventImage;
    }

    public String getEventName() {
        return EventName;
    }

    public void setEventName(String eventName) {
        EventName = eventName;
    }
}
