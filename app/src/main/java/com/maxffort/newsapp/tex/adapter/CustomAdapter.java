package com.maxffort.newsapp.tex.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.maxffort.newsapp.tex.DescribeActivity;
import com.maxffort.newsapp.tex.R;
import com.maxffort.newsapp.tex.helperclasses.DataModel;

import java.util.ArrayList;
import java.util.Calendar;

public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.MyViewHolder> {
    private ArrayList<DataModel> dataSet;
    private Context context;

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView textViewName, dateTextView;
        public CardView trendingCard;
        //        TextView textViewVersion;
        public ImageView imageViewIcon;

        public MyViewHolder(View itemView) {
            super(itemView);
            textViewName = (TextView) itemView.findViewById(R.id.textViewName);
            dateTextView = itemView.findViewById(R.id.uploadTime);
            trendingCard = itemView.findViewById(R.id.trending_card_view);


//            this.textViewVersion = (TextView) itemView.findViewById(R.id.textViewVersion);

            imageViewIcon = (ImageView) itemView.findViewById(R.id.imageView);
        }
    }

    public CustomAdapter(Context context, ArrayList<DataModel> data) {
        this.context = context;
        this.dataSet = data;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.trending_cards, parent, false);




        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {

        Calendar calendar = Calendar.getInstance();
        String currentDate = Integer.toString(calendar.get(Calendar.YEAR)) + "-" + Integer.toString(calendar.get(Calendar.MONTH) + 1) + "-" + Integer.toString(calendar.get(Calendar.DATE));
        final DataModel dataModel = dataSet.get(listPosition);
        holder.textViewName.setText(dataModel.getHeading());


        holder.trendingCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, DescribeActivity.class);

                intent.putExtra("SessionId",100);
                intent.putExtra("newsImageId",dataModel.getImgUrl());
                intent.putExtra("newsImageHeading",dataModel.getHeading());
                intent.putExtra("newsImageDescription",dataModel.getDescription());
                context.startActivity(intent);
            }
        });

        String[] date = dataModel.getDate().split("_");

        if(date[0].equals(currentDate))
            holder.dateTextView.setText("Today");
        else{
            String finalDate[] = date[0].split("-");
            holder.dateTextView.setText(finalDate[2]+"-"+finalDate[1]+"-"+finalDate[0]);
        }
        Glide.with(context).load(dataModel.getImgUrl()).into(holder.imageViewIcon);
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

}
