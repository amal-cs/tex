package com.maxffort.newsapp.tex;

import android.preference.Preference;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class Settings extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);




        if (findViewById(R.id.preferenceContainer) != null){
            if (savedInstanceState != null){
                return;
            }


            getFragmentManager().beginTransaction().add(R.id.preferenceContainer,new SettingsFragment()).commit();
        }


    }
}
