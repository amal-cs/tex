package com.maxffort.newsapp.tex.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.maxffort.newsapp.tex.R;
import com.maxffort.newsapp.tex.helperclasses.Leaderboard;

import java.util.ArrayList;


public class LeaderboardAdapter  extends RecyclerView.Adapter<LeaderboardAdapter.LeaderboardViewHolder>{

    private ArrayList<Leaderboard> leaderboardArrayList;
    private DatabaseReference userNameDatabase;
    private Context context;

    public LeaderboardAdapter(Context context,ArrayList<Leaderboard> leaderboardArrayList){
        this.leaderboardArrayList = leaderboardArrayList;
        this.context = context;
    }

    @NonNull
    @Override
    public LeaderboardViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.leaderboard_list_rows,viewGroup,false);
        return new LeaderboardAdapter.LeaderboardViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final LeaderboardViewHolder leaderboardViewHolder, final int i) {

        leaderboardViewHolder.timeTextView.setText(leaderboardArrayList.get(i).getTime());

        userNameDatabase = FirebaseDatabase.getInstance().getReference("Users/"+leaderboardArrayList.get(i).getUserId()
        +"/Username");
        userNameDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                leaderboardViewHolder.nameTextView.setText(dataSnapshot.getValue().toString());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }

    @Override
    public int getItemCount() {
        return leaderboardArrayList.size();
    }

    public class LeaderboardViewHolder extends RecyclerView.ViewHolder{


        public TextView nameTextView,timeTextView;

        public LeaderboardViewHolder(@NonNull View itemView) {
            super(itemView);
            nameTextView = itemView.findViewById(R.id.nameTextView);
            timeTextView = itemView.findViewById(R.id.timeTextView);

        }
    }
}
