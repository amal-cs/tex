package com.maxffort.newsapp.tex.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.maxffort.newsapp.tex.QuizResult;
import com.maxffort.newsapp.tex.R;
import com.maxffort.newsapp.tex.helperclasses.QuizHistory;

import java.util.ArrayList;

public class QuizHistoryAdapter extends RecyclerView.Adapter<QuizHistoryAdapter.QuizHistoryViewHolder> {

    public ArrayList<QuizHistory> quizHistoryArrayList;
    public Context context;

    public QuizHistoryAdapter(Context context,ArrayList<QuizHistory> quizHistoryArrayList) {
        this.quizHistoryArrayList = quizHistoryArrayList;
        this.context = context;
    }

    @NonNull
    @Override

    public QuizHistoryViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.quizhistory_list_rows,viewGroup,false);
        return new QuizHistoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull QuizHistoryViewHolder quizHistoryViewHolder, int i) {

        QuizHistory quizHistory = quizHistoryArrayList.get(i);
        quizHistoryViewHolder.date.setText(quizHistory.getDate());
        quizHistoryViewHolder.score.setText(quizHistory.getScore());

    }

    @Override
    public int getItemCount() {
        return quizHistoryArrayList.size();
    }

    public class QuizHistoryViewHolder extends RecyclerView.ViewHolder {

        TextView score,date;

        public QuizHistoryViewHolder(@NonNull View itemView) {
            super(itemView);
            date = itemView.findViewById(R.id.dateTextView);
            score = itemView.findViewById(R.id.score);

        }
    }

}
