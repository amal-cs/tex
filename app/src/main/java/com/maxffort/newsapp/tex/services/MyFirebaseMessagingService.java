package com.maxffort.newsapp.tex.services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.maxffort.newsapp.tex.MenuActivity;
import com.maxffort.newsapp.tex.R;
import com.maxffort.newsapp.tex.SnoozeActivity;

import org.json.JSONException;
import org.json.JSONObject;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "MyFirebaseMessaging";


    @Override
    public void onNewToken(String s) {

        Log.d(TAG, "onNewToken: "+s);
        sendRegistrationToServer(s);
    }

    private void sendRegistrationToServer(String s) {
        Log.d(TAG, "sendRegistrationToServer: "+s);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        if (remoteMessage.getData().size()>0){
            Log.d(TAG, "onMessageReceived: "+remoteMessage.getData());
            try {
                JSONObject data = new JSONObject(remoteMessage.getData());
                String jsonMessage = data.getString("extra_information");
                Log.d(TAG, "onMessageReceived: ExtraInformation "+jsonMessage);
            }catch (JSONException e){
                e.printStackTrace();
            }

        }
        if (remoteMessage.getNotification()!= null){
            String title = remoteMessage.getNotification().getTitle();
            String message = remoteMessage.getNotification().getBody();
            String click_action = remoteMessage.getNotification().getClickAction();
            
            sendNotification(title,message,click_action);
        }
    }

    private void sendNotification(String title, String message, String click_action) {
        Intent intent ;
        if (click_action.equals("snoozeActivity")){
            Log.d(TAG, "sendNotification: "+title+" "+message+" "+click_action);
            intent = new Intent(click_action);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);


        }
        else if (click_action.equals("menuActivity")){
            intent = new Intent(this, MenuActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        }
        else
        {
            intent = new Intent(this, MenuActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        }
        PendingIntent pendingIntent = PendingIntent.getActivity(this,0, intent,PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this).setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(title).setContentText(message).setAutoCancel(true).setSound(defaultSoundUri).setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0,notificationBuilder.build());
    }
}
