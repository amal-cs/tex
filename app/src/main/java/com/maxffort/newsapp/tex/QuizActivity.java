package com.maxffort.newsapp.tex;

import android.content.Intent;
import android.graphics.Color;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.ads.consent.ConsentForm;
import com.google.ads.consent.ConsentFormListener;
import com.google.ads.consent.ConsentInfoUpdateListener;
import com.google.ads.consent.ConsentInformation;
import com.google.ads.consent.ConsentStatus;
import com.google.ads.consent.DebugGeography;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.maxffort.newsapp.tex.helperclasses.Question;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

public class QuizActivity extends AppCompatActivity {

    private TextView questionTextView, timerTextView;
    //    private TextView qOne, qTwo, qThree, qFour;
    private int correct, wrong, unanswered, answered;
    private ImageView resultImage;
    private int weekOfYear;
    private RadioGroup quizRadioGroup;
    private CountDownTimer countDownTimer;
    private DatabaseReference questionDatabaseReference, resultDatabaseReference;
    private long startTime, stopTime,totalTime;
    private ArrayList<Integer> questionNumberArrayList = new ArrayList<>();
    private ArrayList<Question> questionArrayList = new ArrayList<>();
    private static final String TAG = "QuizActivity";
    private int i = 0;
    private boolean ansClicked = false,backPressed = false;
    private ConsentForm consentForm;
    private AdView mAdView;
    private CardView ansOne, ansTwo, ansThree, ansFour;
    private Button qOne, qTwo, qThree, qFour;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);
        MobileAds.initialize(this, "ca-app-pub-2174236293887146~4807999939");
        mAdView = new AdView(this);
        mAdView.setAdSize(AdSize.BANNER);
        mAdView.setAdUnitId("ca-app-pub-2174236293887146/7905114177");
        mAdView = findViewById(R.id.adView);

        weekOfYear = getIntent().getIntExtra("weekOfYear",0);

        ansOne = findViewById(R.id.ansOne);
        ansTwo = findViewById(R.id.ansTwo);
        ansThree = findViewById(R.id.ansThree);
        ansFour = findViewById(R.id.ansFour);

        Random random = new Random();
        random.setSeed(System.nanoTime());
        while (questionNumberArrayList.size() < 10) {
            int ran = random.nextInt(100);
            if (!questionNumberArrayList.contains(ran)) {
                questionNumberArrayList.add(ran);
            }
        }

        Log.d(TAG, "onCreate: array size " + questionNumberArrayList);


        ConsentInformation consentInformation = ConsentInformation.getInstance(getApplicationContext());

        consentInformation.requestConsentInfoUpdate(new String[]{"pub-2174236293887146"}, new ConsentInfoUpdateListener() {
            @Override
            public void onConsentInfoUpdated(ConsentStatus consentStatus) {

                if (consentStatus == ConsentStatus.UNKNOWN) {

                    displayConsentForm();
                }
            }

            @Override
            public void onFailedToUpdateConsentInfo(String reason) {
                Log.d(TAG, "onFailedToUpdateConsentInfo: " + reason);
            }
        });

        Bundle extras = new Bundle();
        if (consentInformation.getConsentStatus().equals(ConsentStatus.NON_PERSONALIZED)) {
            extras.putString("npa", "1");
            AdRequest adRequest = new AdRequest.Builder()
                    .addNetworkExtrasBundle(AdMobAdapter.class, extras)
                    .build();
            mAdView.loadAd(adRequest);
        } else {
            AdRequest adRequest = new AdRequest.Builder().build();
            mAdView.loadAd(adRequest);
        }

//        resultImage = findViewById(R.id.answereStatus);
        qOne = findViewById(R.id.qOne);
        qTwo = findViewById(R.id.qTwo);
        qThree = findViewById(R.id.qThree);
        qFour = findViewById(R.id.qFour);
        timerTextView = findViewById(R.id.timerTextView);
        questionTextView = findViewById(R.id.questionTextView);
//        quizRadioGroup = findViewById(R.id.quizRadioGroup);


        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date date = new Date();

        String date1 = simpleDateFormat.format(date);

        questionDatabaseReference = FirebaseDatabase.getInstance().getReference("Quiz/Question/"); //+ date1);//+date1);
//        resultDatabaseReference = FirebaseDatabase.getInstance().getReference("Quiz/Result/08-03-2019");// + date1);//+date1);


//        Toast.makeText(this, simpleDateFormat.format(date), Toast.LENGTH_SHORT).show();


        questionDatabaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (int i : questionNumberArrayList) {
                    questionArrayList.add(new Question(dataSnapshot.child("Question " + i).child("ans").getValue().toString()
                    ,dataSnapshot.child("Question " + i).child("option1").getValue().toString(),
                            dataSnapshot.child("Question " + i).child("option2").getValue().toString()
                    ,dataSnapshot.child("Question " + i).child("option3").getValue().toString()
                    ,dataSnapshot.child("Question " + i).child("option4").getValue().toString(),
                            dataSnapshot.child("Question " + i).child("question").getValue().toString())); //dataSnapshot.child("Question " + i).getValue(Question.class));
                }
//                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
//                    questionArrayList.add(dataSnapshot1.getValue(Question.class));
//                }

                if (questionArrayList.size() == questionNumberArrayList.size()) {
//                    Toast.makeText(QuizActivity.this, "Complete", Toast.LENGTH_SHORT).show();
                    if (questionArrayList.size() > 0)
                        startQuiz(questionArrayList, i);
//                    Log.d(TAG, "onDataChange: " + questionArrayList.get(6).getQuestion());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }

    private void displayConsentForm() {
        consentForm = new ConsentForm.Builder(this, getAppsPrivacyPolicy()).withListener(new ConsentFormListener() {
            @Override
            public void onConsentFormLoaded() {
                super.onConsentFormLoaded();
                consentForm.show();
                Log.d(TAG, "onConsentFormLoaded: ");
            }

            @Override
            public void onConsentFormError(String reason) {
                super.onConsentFormError(reason);
                Log.d(TAG, "onConsentFormError: " + reason);
            }

            @Override
            public void onConsentFormOpened() {
                super.onConsentFormOpened();
            }

            @Override
            public void onConsentFormClosed(ConsentStatus consentStatus, Boolean userPrefersAdFree) {
                super.onConsentFormClosed(consentStatus, userPrefersAdFree);
            }
        }).withPersonalizedAdsOption()
                .withNonPersonalizedAdsOption()
                .build();
        consentForm.load();

    }

    private URL getAppsPrivacyPolicy() {
        URL mUrl = null;
        try {
            mUrl = new URL("https://lanciar.com/policy.pdf");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return mUrl;
    }

    public void startQuiz(ArrayList<Question> questionArrayList, int i) {
        int size = questionArrayList.size();
        startTime = System.currentTimeMillis();
        Log.d(TAG, "startQuiz: counter " + i);

        if (i == 0) {
            Log.d(TAG, "startQuiz: start time " + startTime);
            questionTextView.setText(questionArrayList.get(i).getQuestion());
            qOne.setText(questionArrayList.get(i).getOption1());
            qTwo.setText(questionArrayList.get(i).getOption2());
            qThree.setText(questionArrayList.get(i).getOption3());
            qFour.setText(questionArrayList.get(i).getOption4());
        }
        if (i < size) {
            questionTextView.setText(questionArrayList.get(i).getQuestion());
            qOne.setText(questionArrayList.get(i).getOption1());
            qTwo.setText(questionArrayList.get(i).getOption2());
            qThree.setText(questionArrayList.get(i).getOption3());
            qFour.setText(questionArrayList.get(i).getOption4());
            countDownTimer = quizTimer().start();
        } else if (i == size) {
            Log.d(TAG, "startQuiz: stop time " + stopTime);
            Toast.makeText(this, "Quiz has ended", Toast.LENGTH_SHORT).show();
            exitQuiz();

        }

    }

    public void exitQuiz() {
//        stopTime = System.currentTimeMillis();
        Log.d(TAG, "startQuiz: stop time " + stopTime);
        Intent intent = new Intent(QuizActivity.this, QuizResult.class);
        intent.putExtra("weekOfYear",weekOfYear);
        Log.d(TAG, "exitQuiz: Week of year quiz activity "+weekOfYear);
        intent.putExtra("correct", correct);
        intent.putExtra("answered", answered);
        intent.putExtra("Score", totalTime);
//            intent.putExtra("Date",)

        intent.putExtra("Total", questionArrayList.size());

        Log.d(TAG, "exitQuiz: correct " + correct + " ans " + answered + " total " + questionArrayList.size());
//        Toast.makeText(getApplicationContext(), "correct "+correct+" answered "+answered+" total "+questionArrayList.size(), Toast.LENGTH_SHORT).show();
        startActivity(intent);
        finish();
    }

    public CountDownTimer quizTimer() {
        countDownTimer = new CountDownTimer(10000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                timerTextView.setText(String.valueOf(millisUntilFinished / 1000));
                if (millisUntilFinished / 1000 <= 5) {
                    timerTextView.setTextColor(getResources().getColor(R.color.timerColorRed));
                } else {
                    timerTextView.setTextColor(getResources().getColor(R.color.timerColorGreen));
                }

            }

            @Override
            public void onFinish() {
                if (!backPressed){
                disableGames(true);
                    changeColor();
                    i++;
                    startQuiz(questionArrayList, i);
                    ansClicked = false;
                }

            }
        };
        return countDownTimer;
    }

    public void onRadioButtonClicked(View view) {

        switch (view.getId()) {


            case R.id.qOne:
                disableGames(false);
                stopTime = System.currentTimeMillis();
                totalTime += (stopTime - startTime);
                ansClicked = true;
                if (i < questionArrayList.size()) {
                    answered++;
                    if (questionArrayList.get(i).getAns().equals("1")) {
                        view.setBackgroundColor(getResources().getColor(R.color.timerColorGreen));
                        correct++;
                    } else {
                        setCorrectAns();
                        view.setBackgroundColor(getResources().getColor(R.color.timerColorRed));
                    }
                }
                break;

            case R.id.qTwo:
                disableGames(false);
                stopTime = System.currentTimeMillis();
                totalTime += (stopTime - startTime);
                ansClicked = true;
                if (i < questionArrayList.size()) {
                    answered++;
                    if (questionArrayList.get(i).getAns().equals("2")) {
                        view.setBackgroundColor(getResources().getColor(R.color.timerColorGreen));
                        correct++;
                    } else {
                        setCorrectAns();
                        view.setBackgroundColor(getResources().getColor(R.color.timerColorRed));
                    }
                }
                break;

            case R.id.qThree:
                disableGames(false);
                stopTime = System.currentTimeMillis();
                totalTime += (stopTime - startTime);
                ansClicked = true;
                if (i < questionArrayList.size()) {
                    answered++;
                    if (questionArrayList.get(i).getAns().equals("3")) {
                        view.setBackgroundColor(getResources().getColor(R.color.timerColorGreen));
                        correct++;
                    } else {
                        setCorrectAns();
                        view.setBackgroundColor(getResources().getColor(R.color.timerColorRed));
                    }
                }
                break;

            case R.id.qFour:
                disableGames(false);
                stopTime = System.currentTimeMillis();
                totalTime += (stopTime - startTime);
                ansClicked = true;
                if (i < questionArrayList.size()) {
                    answered++;
                    if (questionArrayList.get(i).getAns().equals("4")) {
                        view.setBackgroundColor(getResources().getColor(R.color.timerColorGreen));
                        correct++;
                    } else {
                        setCorrectAns();
                        view.setBackgroundColor(getResources().getColor(R.color.timerColorRed));

                    }
                }
                break;

            default:
                exitQuiz();

        }


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        backPressed = true;
        exitQuiz();
        finish();
    }

    public void disableGames(Boolean status) {
        qOne.setEnabled(status);
        qTwo.setEnabled(status);
        qThree.setEnabled(status);
        qFour.setEnabled(status);
    }

    public void setCorrectAns() {
        String ans = questionArrayList.get(i).getAns();
        if (ans.equals("1"))
            qOne.setBackgroundColor(Color.GREEN);
        else if (ans.equals("2"))
            qTwo.setBackgroundColor(Color.GREEN);
        else if (ans.equals("3"))
            qThree.setBackgroundColor(Color.GREEN);
        else
            qFour.setBackgroundColor(Color.GREEN);

    }

    public void changeColor() {

        qOne.setBackgroundColor(Color.WHITE);
        qTwo.setBackgroundColor(Color.WHITE);
        qThree.setBackgroundColor(Color.WHITE);
        qFour.setBackgroundColor(Color.WHITE);

    }
}




