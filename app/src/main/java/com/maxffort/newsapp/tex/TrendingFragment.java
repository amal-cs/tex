package com.maxffort.newsapp.tex;


import android.app.ActionBar;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.maxffort.newsapp.tex.adapter.CustomAdapter;
import com.maxffort.newsapp.tex.helperclasses.DataModel;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;


public class TrendingFragment extends Fragment {

    private static CustomAdapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private static RecyclerView recyclerView;
    private ActionBar actionBar;
    private static ArrayList<DataModel> data;
    private RelativeLayout relativeLayout;
    private DatabaseReference databaseReference;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View result = inflater.inflate(R.layout.fragment_trending, container, false);
        recyclerView = (RecyclerView) result.findViewById(R.id.my_recycler_view);
        relativeLayout = result.findViewById(R.id.trendingCoordinateLayout);
        recyclerView.setHasFixedSize(true);

        GradientDrawable gradientDrawable = new GradientDrawable();
        gradientDrawable.setColors(new int[]{
                Color.rgb(0, 201, 255),Color.rgb(146, 254, 157)
        });

        gradientDrawable.setGradientType(GradientDrawable.LINEAR_GRADIENT);
        gradientDrawable.setShape(GradientDrawable.RECTANGLE);

        relativeLayout.setBackground(gradientDrawable);
        data = new ArrayList<DataModel>();

        databaseReference = FirebaseDatabase.getInstance().getReference("Trending");
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                    data.add(new DataModel(dataSnapshot1.child("Data").child("Heading").getValue().toString(), dataSnapshot1.child("Data").child("Image").getValue().toString(), dataSnapshot1.getKey(),
                            dataSnapshot1.child("Data").child("Description").getValue().toString()));
                }


                Calendar calendar = Calendar.getInstance();
                String currentDate = Integer.toString(calendar.get(Calendar.YEAR)) + "-" + Integer.toString(calendar.get(Calendar.MONTH) + 1) + "-" + Integer.toString(calendar.get(Calendar.DATE));


                adapter = new CustomAdapter(getContext(), data);
                layoutManager = new LinearLayoutManager(getContext());
                recyclerView.setLayoutManager(layoutManager);
                recyclerView.setItemAnimator(new DefaultItemAnimator());
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        return result;
    }

}
