package com.maxffort.newsapp.tex.adapter;

import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.ads.consent.ConsentInformation;
import com.google.ads.consent.ConsentStatus;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.maxffort.newsapp.tex.CommentActivity;
import com.maxffort.newsapp.tex.FactCommentActivity;
import com.maxffort.newsapp.tex.R;
import com.maxffort.newsapp.tex.helperclasses.Facts;

import java.util.ArrayList;

public class FactAdapter extends RecyclerView.Adapter {

    private static final String TAG = "FactAdapter";
    private final int ITEM_FACT = 1;
    private final int ITEM_AD = 0;
    private ArrayList<Object> factsArrayList;
    private Context context;
    private DatabaseReference likeDatabaseReference;

    public FactAdapter(Context context, ArrayList<Object> factsArrayList) {
        this.context = context;
        this.factsArrayList = factsArrayList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        switch (i) {
            case ITEM_AD:
                View bannerAd = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.feed_list_ad_rows, viewGroup, false);
                return new AdViewHolder(bannerAd);

            case ITEM_FACT:

            default:
                View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.fact_cards, viewGroup, false);
                return new FactViewHolder(view);

        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int i) {

        int viewType = getItemViewType(i);
        switch (viewType) {
            case ITEM_FACT:
                final Facts facts = (Facts) factsArrayList.get(i);
                final FactAdapter.FactViewHolder factViewHolder = (FactAdapter.FactViewHolder) viewHolder;
                Glide.with(context).load(facts.getImageUrl()).into(factViewHolder.factImageView);
                factViewHolder.factLikeCount.setText(String.valueOf(facts.getLikeCount()));
                isLiked(facts.getFactId(), factViewHolder);

                factViewHolder.factComment.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(context, FactCommentActivity.class);
                        intent.putExtra("FactId", facts.getFactId());
                        intent.putExtra("position", i);
                        context.startActivity(intent);

                        notifyDataSetChanged();

                    }
                });

                factViewHolder.factLike.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        likeDatabaseReference = FirebaseDatabase.getInstance().getReference("Facts/" + facts.getFactId() + "/Likes");
                        likeDatabaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                if (dataSnapshot.child(FirebaseAuth.getInstance().getUid()).exists()) {
                                    factViewHolder.factLike.setImageResource(R.drawable.ic_favorite_border_black_24dp);
                                    likeDatabaseReference.child(FirebaseAuth.getInstance().getUid()).removeValue();
                                    factViewHolder.factLikeCount.setText(String.valueOf(Integer.parseInt(factViewHolder.factLikeCount.getText().toString()) - 1));

                                } else {
                                    factViewHolder.factLike.setImageResource(R.drawable.ic_favorite_black_24dp);
                                    likeDatabaseReference.child(FirebaseAuth.getInstance().getUid()).setValue(1);
                                    factViewHolder.factLikeCount.setText(String.valueOf(Integer.parseInt(factViewHolder.factLikeCount.getText().toString()) + 1));

                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
                    }
                });

                break;

            case ITEM_AD:

            default:
                Log.d(TAG, "onBindViewHolder: ad " + i);
                ConsentInformation consentInformation = ConsentInformation.getInstance(context);
                Bundle extras = new Bundle();
                final AdViewHolder adViewHolder = (AdViewHolder) viewHolder;

                if (consentInformation.getConsentStatus().equals(ConsentStatus.NON_PERSONALIZED)) {
                    extras.putString("npa", "1");
                    AdRequest adRequest = new AdRequest.Builder()
                            .addNetworkExtrasBundle(AdMobAdapter.class, extras)
                            .build();
                    adViewHolder.adView.loadAd(adRequest);

                } else {
                    AdRequest adRequest = new AdRequest.Builder().build();
                    adViewHolder.adView.loadAd(adRequest);
                }

        }

    }

    @Override
    public int getItemViewType(int position) {
        if (factsArrayList.get(position) == null)
            return ITEM_AD;
        else
            return ITEM_FACT;

    }



    public class AdViewHolder extends RecyclerView.ViewHolder {

        public AdView adView;

        public AdViewHolder(@NonNull View itemView) {
            super(itemView);
            adView = itemView.findViewById(R.id.adView);
        }
    }

    @Override
    public int getItemCount() {
        return factsArrayList.size();
    }

    public class FactViewHolder extends RecyclerView.ViewHolder {

        private ImageView factImageView, factLike,factComment;
//        private LinearLayout factLikeLayout;
//        private LinearLayout factCommentLayout;
        private TextView factLikeCount;

        public FactViewHolder(@NonNull View itemView) {
            super(itemView);
            factImageView = itemView.findViewById(R.id.factImage);
//            factLikeLayout = itemView.findViewById(R.id.factLikeLayout);
            factComment = itemView.findViewById(R.id.factComment);
            factLike = itemView.findViewById(R.id.factLike);
            factLikeCount = itemView.findViewById(R.id.factLikeCount);
        }
    }

    public void isLiked(String factId, final FactAdapter.FactViewHolder viewHolder) {
        likeDatabaseReference = FirebaseDatabase.getInstance().getReference("Facts/" + factId + "/Likes");
        final boolean[] status = new boolean[1];
        likeDatabaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (FirebaseAuth.getInstance().getUid() != null)
                    if (dataSnapshot.hasChild(FirebaseAuth.getInstance().getUid())) {
                        viewHolder.factLike.setImageResource(R.drawable.ic_favorite_black_24dp);
                        // Log.d(TAG, "onDataChange: if " + dataSnapshot.hasChild(FirebaseAuth.getInstance().getUid()));
                        status[0] = true;

                    } else {
                        viewHolder.factLike.setImageResource(R.drawable.ic_favorite_border_black_24dp);
                        // Log.d(TAG, "onDataChange: else " + dataSnapshot.hasChild(FirebaseAuth.getInstance().getUid()));
                        status[0] = false;

                    }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
