package com.maxffort.newsapp.tex.helperclasses;

import java.util.Comparator;

public class DataModel {
    private String heading, imgUrl,date,description;

    public DataModel(){}

    public DataModel(String heading, String imgUrl,String date,String description) {
        this.heading = heading;
        this.imgUrl = imgUrl;
        this.date = date;
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setHeading(String heading) {
        this.heading = heading;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getHeading() {
        return heading;
    }

    public String getImgUrl() {
        return imgUrl;
    }
}
