package com.maxffort.newsapp.tex.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearSmoothScroller;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.maxffort.newsapp.tex.CommentActivity;
import com.maxffort.newsapp.tex.DescribeActivity;
import com.maxffort.newsapp.tex.R;
import com.maxffort.newsapp.tex.helperclasses.Comment;

import java.util.ArrayList;

public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.ViewHolder> {

    private ArrayList<Comment> commentArrayList;
    private Context context;

    public CommentAdapter(Context context, ArrayList<Comment> commentArrayList) {
        this.context = context;
        this.commentArrayList = commentArrayList;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.comment_list_rows, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {

        Comment comment = commentArrayList.get(i);
        viewHolder.nameTextView.setText(comment.getName());
        viewHolder.commentTextView.setText(comment.getComment());
        Glide.with(context).load(comment.getUrlImg()).into(viewHolder.userImageView);


    }

    @Override
    public int getItemCount() {
        return commentArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView nameTextView, commentTextView;
        ImageView userImageView;
        public LinearLayout commentLinearLayout;

        ViewHolder(@NonNull View itemView) {
            super(itemView);


            nameTextView = itemView.findViewById(R.id.cUserName);
            commentTextView = itemView.findViewById(R.id.Comment);
            userImageView = itemView.findViewById(R.id.cUserImage);

        }
    }

}
