package com.maxffort.newsapp.tex;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.maxffort.newsapp.tex.adapter.CommentAdapter;
import com.maxffort.newsapp.tex.adapter.FactCommentAdapter;
import com.maxffort.newsapp.tex.helperclasses.Comment;
import com.maxffort.newsapp.tex.helperclasses.FactComment;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;

public class FactCommentActivity extends AppCompatActivity {

    private ArrayList<FactComment> commentArrayList = new ArrayList<>();
    private FactCommentAdapter factCommentAdapter;
    private DatabaseReference databaseReference;
    private EditText factCommentEditText;
    private ImageView factSendCommentButton;
    private int i;
    private GoogleSignInAccount googleSignInAccount;
    private RecyclerView recyclerView;
    private TextView nofactcomment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fact_comment);
        nofactcomment=findViewById(R.id.nofactcomment);
        factCommentEditText = findViewById(R.id.factCommentEditText);
        factSendCommentButton = findViewById(R.id.factSendCommentButton);
        googleSignInAccount = GoogleSignIn.getLastSignedInAccount(getApplicationContext());

        Calendar calendar = Calendar.getInstance();
        final String date = Integer.toString(calendar.get(Calendar.DATE)) + "-" + Integer.toString(calendar.get(Calendar.MONTH) + 1) + "-" + Integer.toString(calendar.get(Calendar.YEAR));

        recyclerView = findViewById(R.id.factCommentRecyclerView);
        String id = getIntent().getStringExtra("FactId");
        i = getIntent().getIntExtra("position", 0);


        databaseReference = FirebaseDatabase.getInstance().getReference("Facts/" + id + "/Comments");
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                    commentArrayList.add(dataSnapshot1.getValue(FactComment.class));
                }
                if (commentArrayList.size() == 0){
                    nofactcomment.setVisibility(View.VISIBLE);
                }

                Collections.reverse(commentArrayList);

                factCommentAdapter = new FactCommentAdapter(FactCommentActivity.this, commentArrayList);
                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
                recyclerView.setLayoutManager(layoutManager);
                recyclerView.setAdapter(factCommentAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        factSendCommentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String comment = factCommentEditText.getText().toString();
                nofactcomment.setVisibility(View.GONE);
                if (!comment.isEmpty() && FirebaseAuth.getInstance().getUid() != null) {
                    pushValues(comment);
                }
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(FactCommentActivity.this, MenuActivity.class);
        intent.putExtra("id", 200);
        intent.putExtra("c", i);
        startActivity(intent);

    }

    private void pushValues(final String comment) {
        DatabaseReference userDatabaseReference = FirebaseDatabase.getInstance().getReference("Users/" + FirebaseAuth.getInstance().getUid());
        userDatabaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    databaseReference.push().setValue(new FactComment(dataSnapshot.child("Username").getValue().toString(), comment, dataSnapshot.child("Image").getValue().toString()));
                    factCommentEditText.setText("");
                    commentArrayList.clear();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
