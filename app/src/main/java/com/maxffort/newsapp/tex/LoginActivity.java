package com.maxffort.newsapp.tex;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CheckedTextView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


public class LoginActivity extends AppCompatActivity {

    private static final String TAG = "LoginActivity";
    private GoogleSignInClient mGoogleSignInClient;
    private Button googleSignInButton;
    private Window window;
    private ImageView iconImageView;
    private ActionBar actionBar;
    private boolean isNew;
    private ProgressBar progressBar;
    private CheckBox privacyPolicyCheckBox;
    private TextView privacyPolicy;
    private DatabaseReference userDatabaseReference;
    private FirebaseAuth firebaseAuth;

    private static final String EMAIL = "email";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        googleSignInButton = findViewById(R.id.gSignin);

        iconImageView = findViewById(R.id.iconImageView);
        privacyPolicy = findViewById(R.id.privacyPolicy);
        privacyPolicyCheckBox = findViewById(R.id.privacyPolicyCheckBox);

        actionBar = getSupportActionBar();
        actionBar.hide();
        window = getWindow();
        window.setStatusBarColor(Color.rgb(0, 0, 0));

        progressBar = findViewById(R.id.pbLoading);

//        GradientDrawable gradientDrawable = new GradientDrawable();
//        gradientDrawable.setColors(new int[]{
//                Color.rgb(0, 201, 255),Color.rgb(146, 254, 157)
//        });
//
//        gradientDrawable.setGradientType(GradientDrawable.LINEAR_GRADIENT);
//        gradientDrawable.setShape(GradientDrawable.RECTANGLE);
////        gradientDrawable.setSize(450, 150);
//        loginLinearLayout.setBackground(gradientDrawable);


        firebaseAuth = FirebaseAuth.getInstance();


        // Configure Google Sign In
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        googleSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (privacyPolicyCheckBox.isChecked())
                    signIn();
                else
                    Toast.makeText(LoginActivity.this, "Please accept the privacy policy", Toast.LENGTH_SHORT).show();
            }
        });


        privacyPolicy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse("http://maxffort.com/policy.pdf");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });


    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode == 100) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                GoogleSignInAccount account = task.getResult(ApiException.class);
                assert account != null;
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                Log.w(TAG, "Google sign in failed", e);
//                updateUI(null);
            }
        }
    }


    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, 100);
        progressBar.setVisibility(ProgressBar.VISIBLE);
    }

    @Override
    protected void onStart() {
        super.onStart();
        FirebaseUser currentUser = firebaseAuth.getCurrentUser();
        updateUI(currentUser);
    }

    private void updateUI(FirebaseUser account) {
        if (account != null) {
            Intent intent = new Intent(LoginActivity.this, MenuActivity.class);
//            intent.putExtra("acitivityId",100);
//            intent.putExtra("newUser",isNew);
            startActivity(intent);
            progressBar.setVisibility(ProgressBar.INVISIBLE);
        }

    }


    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        firebaseAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = firebaseAuth.getCurrentUser();
//                            Toast.makeText(LoginActivity.this, task.getResult().getAdditionalUserInfo().isNewUser()+"test", Toast.LENGTH_SHORT).show();

                            if (task.getResult().getAdditionalUserInfo().isNewUser()) {
                                putNewUserInfo();
//                                isNew = true;
                            }

                            updateUI(user);
                        } else {
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            Toast.makeText(LoginActivity.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                            updateUI(null);
                        }


                    }
                });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishAffinity();
    }

    public void putNewUserInfo() {
        userDatabaseReference = FirebaseDatabase.getInstance().getReference("Users/" + FirebaseAuth.getInstance().getUid());
        userDatabaseReference.child("Likes").setValue(0);
        userDatabaseReference.child("Comments").setValue(0);
    }

}
