package com.maxffort.newsapp.tex;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.ads.consent.ConsentInformation;
import com.google.ads.consent.ConsentStatus;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;


public class DescribeActivity extends AppCompatActivity {


    public ImageView newsImageView;
    private Intent newsIntent;
    private Uri newsUri;
    public TextView newsHeadingView,newsMainDescription,sourceTextView;
    public String newsImageId, newsHeading,newsDescription,newsSourceLink;
    private DatabaseReference databaseReference;
    private static final String TAG = "DescribeActivity";

    public AdView adView;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(DescribeActivity.this,MenuActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_describe);
        adView = new AdView(this);
        adView.setAdSize(AdSize.BANNER);
        adView.setAdUnitId("ca-app-pub-2174236293887146/7905114177");
        adView = findViewById(R.id.describeAd);
        ConsentInformation consentInformation = ConsentInformation.getInstance(getApplicationContext());
        Bundle extras = new Bundle();
        if (consentInformation.getConsentStatus().equals(ConsentStatus.NON_PERSONALIZED)) {
            extras.putString("npa", "1");
            AdRequest adRequest = new AdRequest.Builder()
                    .addNetworkExtrasBundle(AdMobAdapter.class, extras)
                    .build();
            adView.loadAd(adRequest);
        } else {
            AdRequest adRequest = new AdRequest.Builder().build();
            adView.loadAd(adRequest);
        }

        sourceTextView = findViewById(R.id.sourceTextView);
        newsImageView = findViewById(R.id.newsMainImage);
        newsHeadingView = findViewById(R.id.newsMainHeading);
        newsMainDescription = findViewById(R.id.newsMainDescription);

        newsIntent = getIntent();
//        String action = intent.getAction();
        newsUri = newsIntent.getData();

        if (newsUri!=null){
            String path = newsUri.toString();
            String[] splitPath = path.split("=",2);
            Toast.makeText(this, ""+splitPath[1], Toast.LENGTH_SHORT).show();
            databaseReference = FirebaseDatabase.getInstance().getReference(splitPath[1]);
            databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    Glide.with(DescribeActivity.this).load(dataSnapshot.child("Image").getValue().toString()).into(newsImageView);
                    newsHeadingView.setText(dataSnapshot.child("Heading").getValue().toString());
                    newsMainDescription.setText(Html.fromHtml(dataSnapshot.child("Description").getValue().toString()));
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }


        int SessionId = getIntent().getIntExtra("SessionId", 0);
        if (SessionId == 100) {

                newsImageId = getIntent().getStringExtra("newsImageId");
                newsHeading = getIntent().getStringExtra("newsImageHeading");
                newsDescription = getIntent().getStringExtra("newsImageDescription");
                Glide.with(DescribeActivity.this).load(newsImageId).into(newsImageView);
                newsHeadingView.setText(newsHeading);
                newsMainDescription.setText(Html.fromHtml(newsDescription));


        }
        else if (SessionId == 200){

                newsImageId = getIntent().getStringExtra("newsImageId");
                newsHeading = getIntent().getStringExtra("newsImageHeading");
                newsDescription = getIntent().getStringExtra("newsImageDescription");
                Glide.with(DescribeActivity.this).load(newsImageId).into(newsImageView);
                newsHeadingView.setText(newsHeading);
                newsMainDescription.setText(Html.fromHtml(newsDescription));


        }else if(SessionId == 300){

                newsImageId = getIntent().getStringExtra("newsImageId");
                newsHeading = getIntent().getStringExtra("newsImageHeading");
                newsSourceLink = getIntent().getStringExtra("newsLink");
                newsDescription = getIntent().getStringExtra("newsImageDescription");
                Glide.with(DescribeActivity.this).load(newsImageId).into(newsImageView);
                newsHeadingView.setText(newsHeading);
//            sourceTextView.setText(newsSourceLink);
                newsMainDescription.setText(Html.fromHtml(newsDescription));

        }

        sourceTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (newsSourceLink != null){
                    Intent intent = new Intent(DescribeActivity.this,SourceWebView.class);
                    intent.putExtra("link",newsSourceLink);
                    startActivity(intent);

                }
            }
        });

        Calendar c = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMM dd HH:mm:ss yyyy", Locale.US);
        String dates = simpleDateFormat.format(c.getTime());
        Log.d(TAG, "onCreate: Date "+dates);
        


    }
}
