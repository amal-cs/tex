package com.maxffort.newsapp.tex;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.ads.consent.ConsentInformation;
import com.google.ads.consent.ConsentStatus;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.nio.BufferUnderflowException;
import java.util.Calendar;

public class SnoozeActivity extends AppCompatActivity {

    private FloatingActionButton snoozeBtn;
    private AdView snoozeAd;
    private Boolean clicked = false;
    private AlertDialog alertDialog;
    CharSequence[] times = {"10 minutes","30 minutes","1 hour"};
    private String heading;
    private DatabaseReference databaseReference;
    private TextView headingTextView,descriptionTextView,snoozeSourceTextView;
    private ImageView imgView;
    String title = "",description="",imageUrl="",link;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_snooze);
        headingTextView = findViewById(R.id.snoozeHeading);
        descriptionTextView = findViewById(R.id.snoozeDescription);
        descriptionTextView.setMovementMethod(new ScrollingMovementMethod());
        snoozeSourceTextView = findViewById(R.id.snoozeSourceTextView);
        imgView = findViewById(R.id.snoozeNewsImage);


        snoozeAd = new AdView(this);
        snoozeAd.setAdSize(AdSize.BANNER);
        snoozeAd.setAdUnitId("ca-app-pub-2174236293887146/7905114177");
        snoozeAd = findViewById(R.id.snoozeAd);
        ConsentInformation consentInformation = ConsentInformation.getInstance(getApplicationContext());
        Bundle extras = new Bundle();
        if (consentInformation.getConsentStatus().equals(ConsentStatus.NON_PERSONALIZED)) {
            extras.putString("npa", "1");
            AdRequest adRequest = new AdRequest.Builder()
                    .addNetworkExtrasBundle(AdMobAdapter.class, extras)
                    .build();
            snoozeAd.loadAd(adRequest);
        } else {
            AdRequest adRequest = new AdRequest.Builder().build();
            snoozeAd.loadAd(adRequest);
        }



        descriptionTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!clicked){
                    clicked = true;
                    snoozeBtn.hide();
                }else{
                    clicked = false;
                    snoozeBtn.show();
                }
            }
        });

        if(getIntent().getExtras()!=null){
            Object value;
            for (String key : getIntent().getExtras().keySet()){
                if (key.equals("extra_information")){
                    value = getIntent().getExtras().get(key);
                    heading = value.toString();
                }
            }
        }
        String notifyTitle = getIntent().getStringExtra("notificationHeading");
        if (notifyTitle != null){
            heading = notifyTitle;
        }
//        Toast.makeText(this, "noti"+heading, Toast.LENGTH_SHORT).show();
       //® heading = "This is a sample heading used for testing purpose only";     //Remove after testing
        Calendar calendar = Calendar.getInstance();
        String date = Integer.toString(calendar.get(Calendar.DATE)) + "-" + Integer.toString(calendar.get(Calendar.MONTH) + 1) + "-" + Integer.toString(calendar.get(Calendar.YEAR));
        databaseReference = FirebaseDatabase.getInstance().getReference("News/"+heading);

        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                        title = dataSnapshot.child("Data").child("Heading").getValue().toString();
                        description = dataSnapshot.child("Data").child("Description").getValue().toString();
                        imageUrl = dataSnapshot.child("Data").child("Image").getValue().toString();
                        link = dataSnapshot.child("Data").child("Link").getValue().toString();
//                        Toast.makeText(SnoozeActivity.this, ""+title, Toast.LENGTH_SHORT).show();


                headingTextView.setText(title);
                descriptionTextView.setText(description);
                Glide.with(getApplicationContext()).load(imageUrl).into(imgView);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        snoozeSourceTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (link != null){
                    Intent intent = new Intent(SnoozeActivity.this,SourceWebView.class);
                    intent.putExtra("link",link);
                    startActivity(intent);

                }
            }
        });

        snoozeBtn = findViewById(R.id.snoozeBtn);
        snoozeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createAlertDialog();
            }
        });

    }

    private void scheduleNotification(int delay){

        Intent notificationIntent = new Intent(this,NotificationPublishers.class);
        notificationIntent.putExtra(NotificationPublishers.NOTIFICATION_TITLE,title);
        notificationIntent.putExtra(NotificationPublishers.NOTIFICATION_ID,heading);
        notificationIntent.putExtra(NotificationPublishers.NOTIFICATION_DESCRIPTION,description);
//        notificationIntent.putExtra(NotificationPublishers.NOTIFICATION,notification);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this,100,notificationIntent,0);

        long futureInMillis = System.currentTimeMillis() + delay;
//        Toast.makeText(this, "Time"+futureInMillis, Toast.LENGTH_SHORT).show();
        AlarmManager alarmManager = (AlarmManager)getSystemService(ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP, futureInMillis, pendingIntent);
    }


    private void createAlertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(SnoozeActivity.this);
        builder.setTitle("Select your choice");
        builder.setSingleChoiceItems(times, -1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                switch (i){
                    case 0:
                        Toast.makeText(SnoozeActivity.this, "Snoozed for 10 min", Toast.LENGTH_SHORT).show();
                        scheduleNotification(600000);
                        break;
                    case 1:
                        Toast.makeText(SnoozeActivity.this, "Snoozed for 30 min", Toast.LENGTH_SHORT).show();
                        scheduleNotification(1800000);
                        break;
                    case 2:
                        Toast.makeText(SnoozeActivity.this, "Snoozed for 1 hour", Toast.LENGTH_SHORT).show();
                        scheduleNotification(3600000);
                        break;
                }
                alertDialog.dismiss();
            }
        });
        alertDialog = builder.create();
        alertDialog.show();
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
//            Toast.makeText(SnoozeActivity.this, "Title"+intent.getExtras().getString("title"), Toast.LENGTH_SHORT).show();
        }
    };

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent mainIntent = new Intent(getApplicationContext(),MenuActivity.class);
        startActivity(mainIntent);
        finish();
    }
}
