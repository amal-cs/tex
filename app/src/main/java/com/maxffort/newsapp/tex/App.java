package com.maxffort.newsapp.tex;

import android.app.Application;

import com.bumptech.glide.request.target.ViewTarget;
import com.google.firebase.database.FirebaseDatabase;

public class App extends Application {
    @Override public void onCreate() {
        super.onCreate();
        ViewTarget.setTagId(R.id.glide_tag);
//        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
    }
}
