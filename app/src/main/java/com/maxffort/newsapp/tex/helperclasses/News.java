package com.maxffort.newsapp.tex.helperclasses;

public class News {

    private String Heading,Image,id,description;
    private long likeCount;

    public News(){

    }

    public News(String heading,String Image,String id,long likeCount,String description){
        this.Heading = heading;
        this.Image = Image;
        this.description = description;
        this.id = id;
        this.likeCount = likeCount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setLikeCount(long likeCount) {
        this.likeCount = likeCount;
    }

    public long getLikeCount() {
        return likeCount;
    }

    public String getHeading() {
        return Heading;
    }

    public void setHeading(String heading) {
        this.Heading = heading;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String Image) {
        this.Image = Image;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }
}
