package com.maxffort.newsapp.tex;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.maxffort.newsapp.tex.adapter.EventAdapter;
import com.maxffort.newsapp.tex.helperclasses.Events;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;


/**
 * A simple {@link Fragment} subclass.
 */
public class EventFragment extends Fragment {
    private RecyclerView eventRecyclerView;
    private EventAdapter eventAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private ArrayList<Events> eventsArrayList;
    private TextView noevent;
    private DatabaseReference databaseReference;


    public EventFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_event, container, false);
        eventRecyclerView = view.findViewById(R.id.eventRecyclerView);
        noevent = view.findViewById(R.id.noevent);

        eventsArrayList = new ArrayList<>();
        databaseReference = FirebaseDatabase.getInstance().getReference("Events");
        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()){
                    eventsArrayList.add(new Events(dataSnapshot1.child("EventDescription").getValue().toString(),dataSnapshot1.child("EventImage").getValue().toString(),dataSnapshot1.child("EventName").getValue().toString(),dataSnapshot1.child("EventTime").getValue().toString()));

                }
                if (eventsArrayList.size()>0){
                    noevent.setVisibility(View.INVISIBLE);
                }

                Collections.sort(eventsArrayList, new Comparator<Events>(){

                    @Override
                    public int compare(Events o1, Events o2) {
                        return o1.getEventTime().compareTo(o2.getEventTime());
                    }
                });

                if (eventsArrayList.size() == dataSnapshot.getChildrenCount()){
                    eventAdapter = new EventAdapter(getContext(),eventsArrayList);
                    layoutManager = new LinearLayoutManager(getContext());
                    eventRecyclerView.setLayoutManager(layoutManager);
                    eventRecyclerView.setItemAnimator(new DefaultItemAnimator());
                    eventRecyclerView.setAdapter(eventAdapter);
                }


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        return view;
    }

}
