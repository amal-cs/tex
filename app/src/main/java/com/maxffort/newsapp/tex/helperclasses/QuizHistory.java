package com.maxffort.newsapp.tex.helperclasses;

import com.maxffort.newsapp.tex.QuizResult;

public class QuizHistory {

    private String date,score;

    public QuizHistory(String date,String score){
        this.date = date;
        this.score = score;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
