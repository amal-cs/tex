package com.maxffort.newsapp.tex;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.load.data.DataRewinder;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class QuizResult extends AppCompatActivity {

    private TextView unansweredTextView,answeredTextView,correctTextView,wrongTextView,scoreTextView;
    private DatabaseReference quizUserResultDatabaseReference,leaderboardDatabaseReference;
    private ImageView resultImageView;
    private int weekOfYear;
    private static final String TAG = "QuizResult";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_result);

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();

        resultImageView = findViewById(R.id.resultImg);

        unansweredTextView = findViewById(R.id.unansweredTextView);
        answeredTextView = findViewById(R.id.answeredTextView);
        correctTextView = findViewById(R.id.correctTextView);
        wrongTextView = findViewById(R.id.wrongTextView);
        scoreTextView = findViewById(R.id.scoreTextView);



        int correct = getIntent().getIntExtra("correct",0);
        int answered = getIntent().getIntExtra("answered",0);
        long score = getIntent().getLongExtra("Score",0);
        int total = getIntent().getIntExtra("Total",0);
        int weekOfYear = getIntent().getIntExtra("weekOfYear",0);

        Log.d(TAG, "pushResult: Week of year "+weekOfYear);
        quizUserResultDatabaseReference = FirebaseDatabase.getInstance().getReference("Users/"+FirebaseAuth.getInstance().getUid()+"/Quiz/"+simpleDateFormat.format(date));
        leaderboardDatabaseReference = FirebaseDatabase.getInstance().getReference("Quiz/Leaderboard/"+weekOfYear);

    if (correct == 0){
        resultImageView.setImageResource(R.drawable.fail);
    }
    else if (correct < 5 && correct > 0){
        resultImageView.setImageResource(R.drawable.bronze);
    }else if(correct >= 5 && correct < 10)
        resultImageView.setImageResource(R.drawable.silver);
    else
        resultImageView.setImageResource(R.drawable.gold);


        wrongTextView.setText(String.valueOf(total - correct));
        unansweredTextView.setText(String.valueOf(total - answered));
        answeredTextView.setText(String.valueOf(answered));
        correctTextView.setText(String.valueOf(correct));
        scoreTextView.setText(String.valueOf(pushResult(correct,score)));

//        quizUserResultDatabaseReference.child()


    }

    public double pushResult(int correct,long time){


        final double result;
        double relativeTime;
        Log.d(TAG, "pushResult: correct "+correct+" time "+time);
        relativeTime = (100000 - time)/(1000);
        result = Math.abs(relativeTime * correct);


        quizUserResultDatabaseReference.setValue(result);
        leaderboardDatabaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.child(FirebaseAuth.getInstance().getUid()).exists()){
                    Log.d(TAG, "onDataChange: leader"+dataSnapshot.child(FirebaseAuth.getInstance().getUid()).getValue()+"datasnap"+dataSnapshot);
                   leaderboardDatabaseReference.child(FirebaseAuth.getInstance().getUid()).setValue(Integer.parseInt(dataSnapshot.child(FirebaseAuth.getInstance().getUid()).getValue().toString()) + result);
                }else
                    leaderboardDatabaseReference.child(FirebaseAuth.getInstance().getUid()).setValue(result);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        return result;
    }

}
