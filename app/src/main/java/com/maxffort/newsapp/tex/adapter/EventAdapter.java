package com.maxffort.newsapp.tex.adapter;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.maxffort.newsapp.tex.BroadcastClass;
import com.maxffort.newsapp.tex.R;
import com.maxffort.newsapp.tex.helperclasses.Events;

import java.util.ArrayList;
import java.util.Calendar;

import static android.content.Context.ALARM_SERVICE;


public class EventAdapter extends RecyclerView.Adapter<EventAdapter.ViewHolder> {

    private ArrayList<Events> eventsArrayList;
    private Context context;

    public EventAdapter(Context context, ArrayList<Events> eventsArrayList){
        this.context = context;
        this.eventsArrayList = eventsArrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.event_cards,viewGroup,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {

        final Events events = eventsArrayList.get(i);
        viewHolder.eventHeading.setText(events.getEventName());
        viewHolder.eventDescripiton.setText(events.getEventDescription());
        Glide.with(context).load(events.getEventImage()).into(viewHolder.eventImage);
        String[] time = events.getEventTime().split("-");
        String finalTime = time[2]+"-"+time[1]+"-"+time[0];
        viewHolder.eventTextView.setText(finalTime);
        viewHolder.eventAlarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar calendar = Calendar.getInstance();

                setAlarmManager(events.getEventTime(),events.getEventName());
            }

        });
    }

    @Override
    public int getItemCount() {
        return eventsArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private ImageView eventImage;
        private Button eventAlarm;
        private TextView eventHeading,eventDescripiton,eventTextView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            eventTextView = itemView.findViewById(R.id.eventTextView);
            eventDescripiton = itemView.findViewById(R.id.eventDescription);
            eventImage = itemView.findViewById(R.id.eventImage);
            eventAlarm = itemView.findViewById(R.id.eventAlarm);
            eventHeading = itemView.findViewById(R.id.eventHeading);
        }
    }

    public void setAlarmManager(String date,String heading) {


        String[] dateS = date.split("-");
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(dateS[3]));
        calendar.set(Calendar.MONTH, Integer.parseInt(dateS[1]) - 1);
        calendar.set(Calendar.YEAR, Integer.parseInt(dateS[0]));
        calendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(dateS[2]));
        calendar.set(Calendar.MINUTE, Integer.parseInt(dateS[4]));

        Intent intent = new Intent(context, BroadcastClass.class);
        intent.putExtra("Heading",heading);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 100, intent, 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
        Toast.makeText(context, "Alert set ", Toast.LENGTH_SHORT).show();

    }


}
